package metromm.data;

import javafx.scene.text.Font;

public interface MapElement {
	void setFont(String family, boolean bold, boolean italics, int size);

	Font getFont();

	void drag(double x, double y);

	void finishDrag(double x, double y);

	void select(boolean selecting);

	NodeType getType();

	enum NodeType {
		LINE_END,
		LINE,
		LABEL,
		IMAGE,
		STATION,

	}

	enum Direction {
		DOWN, LEFT, UP, RIGHT
	}
}
