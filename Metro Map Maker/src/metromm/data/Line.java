package metromm.data;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polyline;
import javafx.scene.text.Font;
import metromm.file.JSONProps;

import java.util.ArrayList;

import static metromm.file.JSONProps.JSONProp.STATION_RADIUS;

public class Line extends Polyline implements MapElement {
	private Map map;
	private ArrayList<String> stations;
	public LineEnd start, end;
	private boolean circular;

	public void setThickness(double thickness) {
		setStrokeWidth(thickness);
	}

	public String getName() {
		return name;
	}

	public Color getColor() {
		return color;
	}

	private String name;
	private Color color;

	public Line(String name, Color lineColor, double startX, double startY, double thickness, Map map, Color lineEndTextColor) {
		this.map = map;
		this.name = name;
		this.color = lineColor;
		start = new LineEnd(startX, startY, this, JSONProps.getInstance().getNum(STATION_RADIUS), lineEndTextColor);
		start.setFill(lineColor);
		stations = new ArrayList<>();
		setStrokeWidth(thickness);
		setStroke(lineColor);
		start.radiusProperty().bind(strokeWidthProperty().multiply(2));
		circular = false;
	}

	public void finishLine(double endX, double endY, Color lineEndTextColor) {
		end = new LineEnd(endX, endY, this, JSONProps.getInstance().getNum(STATION_RADIUS), lineEndTextColor);
		end.setFill(color);
		end.radiusProperty().bind(strokeWidthProperty().multiply(2));
		getPoints().addAll(start.getX(), start.getY(), endX, endY);

	}

	public void addStation(String closest, Station toAdd) {
		if (stations.contains(toAdd.getName()))
			return;
		if (stations.isEmpty()) {
			stations.add(toAdd.getName());
		} else {
			stations.add(stations.indexOf(closest), toAdd.getName());
		}
		updateLine();
	}

	public void makeCircular(boolean circ) {
		if (circ && !circular) {
			end.centerXProperty().bindBidirectional(start.centerXProperty());
			end.centerYProperty().bindBidirectional(start.centerYProperty());
			end.getLabel().layoutXProperty().bindBidirectional(start.getLabel().layoutXProperty());
			end.getLabel().layoutYProperty().bindBidirectional(start.getLabel().layoutYProperty());
			circular = true;
		} else if (!circ && circular) {
			end.centerXProperty().unbindBidirectional(start.centerXProperty());
			end.centerYProperty().unbindBidirectional(start.centerYProperty());
			end.getLabel().layoutXProperty().unbindBidirectional(start.getLabel().layoutXProperty());
			end.getLabel().layoutYProperty().unbindBidirectional(start.getLabel().layoutYProperty());
			circular = false;
		} else return;
		updateLine();

	}

	public boolean isCircular() {
		return circular;
	}


	public ArrayList<String> getStations() {
		return stations;
	}

	public void updateLine(){
		getPoints().clear();
		getPoints().addAll(start.getX(), start.getY());
		stations.forEach(staName -> {
			Station sta = map.getStation(staName);
			getPoints().addAll(sta.getCenterX(), sta.getCenterY());
		});
		getPoints().addAll(end.getX(), end.getY());
	}

	public NodeType getType() {
		return NodeType.LINE;
	}

	public double getThickness() {
		return getStrokeWidth();
	}

	@Override
	public void setFont(String family, boolean bold, boolean italics, int size) {
		start.setFont(family, bold, italics, size);
		end.setFont(family, bold, italics, size);
	}

	public void setFont(Font font) {
		start.setFont(font);
		end.setFont(font);
	}

	@Override
	public Font getFont() {
		return start.getFont();
	}

	@Override
	public void drag(double x, double y) {

	}

	@Override
	public void finishDrag(double x, double y) {

	}

	@Override
	public void select(boolean selected) {
		start.change(selected);
		end.change(selected);
		/*stations.forEach(staName -> {
			Station sta = map.getStation(staName);
			if (selected)
				sta.setEffect(CHOSEN_EFFECT);
			else
				sta.setEffect(null);
		});*/
	}

	public Node[] getNodes() {
		if (end != null)
			return new Node[]{this, start.getLabel(), start, end.getLabel(), end};
		return new Node[]{this, start.getLabel(), start};
	}

	public void setName(String name) {
		this.name = name;
		start.getLabel().setText(name);
		end.getLabel().setText(name);
	}

	public void setColor(Color color) {
		this.color = color;
		start.setFill(color);
		end.setFill(color);
		setStroke(color);
	}

	public String getClosestStation(double x, double y) {
		if (stations.isEmpty()) {
			return null;
		}
		int closestIndex = 0;
		double closestDistance = Double.MAX_VALUE;
		for (int i = 0; i < stations.size(); i++) {
			Station station = map.getStation(stations.get(i));
			Point2D pnt = new Point2D(station.getCenterX(), station.getCenterY());
			double dist = pnt.distance(x, y);
			if (dist < closestDistance) {
				closestDistance = dist;
				closestIndex = i;
			}
		}
		return stations.get(closestIndex);
	}

	public LineEnd getOtherEnd(LineEnd obj) {
		return obj == start ? end : start;
	}

	public Color[] getTextColors() {
		return new Color[]{start.getTextColor(), end.getTextColor()};
	}
}
