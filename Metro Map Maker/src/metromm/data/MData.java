package metromm.data;

import djf.components.AppDataComponent;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.paint.Color;
import metromm.MetroApp;
import metromm.gui.MWorkspace;

import java.util.Stack;

public class MData implements AppDataComponent {
	//Set up the static highlight effect for the selected node
	static {
		DropShadow dropShadowEffect = new DropShadow();
		dropShadowEffect.setOffsetX(0.0f);
		dropShadowEffect.setOffsetY(0.0f);
		dropShadowEffect.setSpread(1.0);
		dropShadowEffect.setColor(Color.YELLOW);
		dropShadowEffect.setBlurType(BlurType.GAUSSIAN);
		dropShadowEffect.setRadius(15);
		CHOSEN_EFFECT = dropShadowEffect;
	}

	public static Effect CHOSEN_EFFECT;
	private MetroApp app;
	private Stack<Map> actionsToUndo, actionsToRedo;
	private Map currentMap;
	private MapElement selected;
	private SimpleObjectProperty<MState> state = new SimpleObjectProperty<>(this, "state");
	private ObservableList<Node> allNodes;
	private ListChangeListener<Node> listListener;

	public MData(MetroApp app) {
		this.app = app;
	}

	public void newMap(String name) {
		setMap(new Map(name), false);
	}

	public Map setMap(Map map, boolean isUndoRedo) {
		if (!isUndoRedo) {
			actionsToUndo = new Stack<>();
			actionsToRedo = new Stack<>();
		}
		this.currentMap = map;
		setState(MState.SELECTING);
		bindLists(null, true);
		if (allNodes == null)
			setNodeList(app.getWorkspaceComponent().getCanvas().getChildren());
		allNodes.clear();
		allNodes.setAll(currentMap.getAllNodes());
		bindLists(currentMap.getAllNodes(), false);
		app.getWorkspaceComponent().reloadWorkspace(this);
		app.getWorkspaceComponent().loadBackgroundSettings(map);
		app.getWorkspaceComponent().setMapSize(map.getWidth(), map.getHeight());
		app.getWorkspaceComponent().popCombos();
		app.getWorkspaceComponent().enableButtons();
		return map;
	}

	public void undo() {
		if (!actionsToUndo.isEmpty()) {
			deselect();
			actionsToRedo.push(currentMap.deepCopy());
			setMap(actionsToUndo.pop(), true);
		}
	}

	public void redo() {
		if (!actionsToRedo.isEmpty()) {
			deselect();
			actionsToUndo.push(currentMap.deepCopy());
			setMap(actionsToRedo.pop(), true);
		}
	}

	/**
	 * This function would be called when initializing data.
	 */
	@Override
	public void resetData() {
		newMap("Unnamed");
	}

	public Map getMap() {
		return currentMap;
	}

	public MapElement getSelected() {
		return selected;
	}

	public MetroApp getApp() {
		return app;
	}

	public void setState(MState state) {
		this.state.set(state);
	}

	public MState getState() {
		return state.get();
	}

	public ObjectProperty<MState> stateProperty() {
		return state;
	}

	public void addNewLine(Line newLine) {
		currentMap.addLine(newLine);
		allNodes.addAll(newLine.getNodes());
		app.getWorkspaceComponent().reloadWorkspace(this);
	}

	public void finishNewLine(Line newLine) {
		allNodes.removeAll(newLine.getNodes());
		allNodes.addAll(newLine.getNodes());
		select(newLine);
	}

	public void deselect() {
		if (selected != null) {
			selected.select(false);
			selected = null;
		}
		setState(MState.SELECTING);
		app.getWorkspaceComponent().enableButtons();
	}

	public void snap() {
		if (actionsToRedo != null) {
			actionsToUndo.push(currentMap.deepCopy());
			actionsToRedo.clear();
			app.getWorkspaceComponent().enableButtons();
		}
	}

	public boolean canUndo() {
		return !actionsToUndo.isEmpty();
	}

	public boolean canRedo() {
		return !actionsToRedo.isEmpty();
	}

	public void select(MapElement element) {
		if (selected == element) return;
		deselect();
		element.select(true);
		selected = element;
		setState(MState.DRAGGING);
		app.getWorkspaceComponent().loadSettings(selected);
	}

	public Line selectLine(String name) {
		Line line = null;
		for (Line l : currentMap.getLines()) {
			if (l.getName().equals(name)) {
				line = l;
				break;
			}
		}
		if (line != null) {
			select(line);
		}
		return line;
	}

	public void selectStation(String name) {
		Station sta = null;
		for (Station s : currentMap.getAllStations()) {
			if (s.getName().equals(name)) {
				sta = s;
				break;
			}
		}
		if (sta != null)
			select(sta);
	}

	public MapElement selectTopElement(double x, double y) {
		MapElement element = getTopElement(x, y);
		if (element == selected)
			return element;
		deselect();
		if (element != null) {
			element.select(true);
			MWorkspace workspace = app.getWorkspaceComponent();
			workspace.loadSettings(element);
			element.drag(x, y);
			setState(MState.DRAGGING);
		} else
			setState(MState.SELECTING);
		selected = element;
		return element;
	}

	public MapElement getTopElement(double x, double y) {
		for (int i = allNodes.size() - 1; i >= 0; i--) {
			Node node = allNodes.get(i);
			if (node.contains(x, y) && !(node instanceof Line)) {
				return (MapElement) node;
			}
		}
		return null;
	}

	public void setNodeList(ObservableList<Node> nodeList) {
		this.allNodes = nodeList;
	}

	public void addStation(Station newStation) {
		currentMap.addStation(newStation);
		allNodes.addAll(newStation, newStation.getLabel());
	}

	public void removeStation(Station station) {
		currentMap.getLines().forEach(line -> {
			if (line.getStations().contains(station.getName())) {
				line.getStations().remove(station.getName());
				line.updateLine();
			}
		});
		currentMap.getAllStations().remove(station);
		allNodes.removeAll(station, station.getLabel());
		app.getWorkspaceComponent().loadSettings(station);
	}

	public void bindLists(ObservableList<Node> mapList, boolean clear) {
		if (listListener != null)
			allNodes.removeListener(listListener);
		if (clear) return;
		ListChangeListener<Node> newListener = change -> {
			if (change.next()) {
				change.getRemoved().forEach(mapList::remove);
				//noinspection UseBulkOperation
				change.getAddedSubList().forEach(mapList::add);
			}
		};
		allNodes.addListener(newListener);
		listListener = newListener;
	}

	public void addOther(MapElement element) {
		currentMap.addOther(element);
		allNodes.add((Node) element);
	}

	public void removeElement(MapElement element) {
		allNodes.remove(element);
		currentMap.getOtherElements().remove(element);
		deselect();
	}


	public enum MState {
		SELECTING,
		DRAGGING,
		DRAGGING_NOTHING,
		ADD_LINE,
		FINISH_LINE,
		ADD_STATION,
		ADD_IMAGE,
		ADD_LABEL,
		ADD_STA_TO_LINE,
		REM_STA_FROM_LINE,
	}
}