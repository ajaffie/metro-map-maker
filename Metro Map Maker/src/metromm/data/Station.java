package metromm.data;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import java.util.ArrayList;

import static metromm.data.MData.CHOSEN_EFFECT;

public class Station extends Circle implements MapElement {


	private Text label;
	private ArrayList<Line> lines;
	private Color color;
	private Direction labelDirection = Direction.DOWN;
	private Color textColor;

	public int getDirIndex() {
		return dirIndex;
	}

	public void setDirIndex(int dirIndex) {
		this.dirIndex = dirIndex;
		labelDirection = Direction.values()[dirIndex];
	}

	private int dirIndex = 0;

	public Station(double x, double y, double radius, String label, Color color, Color textColor) {
		super(radius);
		lines = new ArrayList<>();
		this.label = new Text(label);
		this.color = color;
		setFill(color);
		this.label.layoutXProperty().bind(centerXProperty());
		this.label.layoutYProperty().bind(centerYProperty());
		setPosition(x, y);
		setTextColor(textColor);

	}

	public void addLine(Line line) {
		lines.add(line);
	}

	public void removeLine(Line line) {
		lines.remove(line);
	}

	public void setPosition(double x, double y) {
		setCenterX(x);
		setCenterY(y);
		updateLines();
		updateLabelPos();
	}

	private void updateLines() {
		lines.forEach(Line::updateLine);
	}

	public void updateLabelPos() {
		switch (labelDirection) {

			case UP:
				label.setTranslateX(-label.getText().length() * getFont().getSize() / 4.15);
				label.setTranslateY(-(getRadius() + 5));
				break;
			case DOWN:
				label.setTranslateY(getRadius() + 5 + getFont().getSize() / 2);
				label.setTranslateX(-label.getText().length() * getFont().getSize() / 4.15);
				break;
			case LEFT:
				label.setTranslateY(3);
				label.setTranslateX(-getRadius() - 3 - label.getText().length() * getFont().getSize() / 2.05);
				break;
			case RIGHT:
				label.setTranslateY(3);
				label.setTranslateX(getRadius() + 5);
				break;
		}
	}

	public void toggleDirection() {
		if (dirIndex == 3)
			dirIndex = 0;
		else
			dirIndex++;
		labelDirection = Direction.values()[dirIndex];
		updateLabelPos();
	}


	public Text getLabel() {
		return label;
	}

	@Override
	public void setFont(String family, boolean bold, boolean italics, int size) {
		label.setFont(Font.font(family, bold ? FontWeight.BOLD : FontWeight.NORMAL, italics ? FontPosture.ITALIC : FontPosture.REGULAR, size));
	}

	public void setFont(Font font) {
		label.setFont(font);
	}

	@Override
	public Font getFont() {
		return label.getFont();
	}

	@Override
	public void drag(double x, double y) {
		setPosition(x, y);
	}

	@Override
	public void finishDrag(double x, double y) {
		updateLines();
	}

	@Override
	public void select(boolean selecting) {
		if (selecting)
			setEffect(CHOSEN_EFFECT);
		else
			setEffect(null);
	}

	@Override
	public NodeType getType() {
		return NodeType.STATION;
	}

	public void addToLine(String closest, Line line) {
		this.lines.add(line);
		line.addStation(closest, this);
	}

	public String getName() {
		return label.getText();
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
		setFill(color);
	}

	public Color getTextColor() {
		return textColor;
	}

	public void setTextColor(Color textColor) {
		this.textColor = textColor;
		label.setFill(textColor);
	}
}
