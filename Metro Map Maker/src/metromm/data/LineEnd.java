package metromm.data;

import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;

import static metromm.data.MData.CHOSEN_EFFECT;

public class LineEnd extends Circle implements MapElement {

	private Color textColor;

	public Label getLabel() {
		return label;
	}

	private Label label;
	private Line owner;

	public LineEnd(double x, double y, Line owner, double radius, Color textColor) {
		super(radius);
		label = new Label(owner.getName());
		label.setVisible(false);
		setPosition(x, y);
		this.owner = owner;
		setTextColor(textColor);
	}

	public void setPosition(double x, double y) {
		label.setLayoutX(x);
		label.setLayoutY(y);
		setCenterX(x);
		setCenterY(y);

	}

	public double getX() {
		return getCenterX();
	}

	public double getY() {
		return getCenterY();
	}

	@Override
	public NodeType getType() {
		return NodeType.LINE_END;
	}


	@Override
	public void setFont(String family, boolean bold, boolean italics, int size) {
		label.setFont(Font.font(family, bold ? FontWeight.BOLD : FontWeight.NORMAL, italics ? FontPosture.ITALIC : FontPosture.REGULAR, size));

	}

	@Override
	public Font getFont() {
		return label.getFont();
	}

	@Override
	public void drag(double x, double y) {
		setPosition(x, y);
		owner.updateLine();
	}

	@Override
	public void finishDrag(double x, double y) {
		if (getBoundsInParent().intersects(owner.getOtherEnd(this).getBoundsInParent())) {
			drag(owner.getOtherEnd(this).getX(), owner.getOtherEnd(this).getY());
		}
	}

	@Override
	public void select(boolean selecting) {
		change(selecting);
	}

	public String getName() {
		return owner.getName();
	}

	public Line getLine() {
		return owner;
	}

	public void change(boolean dragcircles) {
		setVisible(dragcircles);
		label.setVisible(!dragcircles);
		if (dragcircles)
			setEffect(CHOSEN_EFFECT);
		else
			setEffect(null);
	}

	public void setFont(Font font) {
		label.setFont(font);
	}

	public Color getTextColor() {
		return textColor;
	}

	public void setTextColor(Color textColor) {
		this.textColor = textColor;
		label.setTextFill(textColor);
	}
}
