package metromm.data;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

import static metromm.data.MData.CHOSEN_EFFECT;


public class MapImage extends Rectangle implements MapElement {
	private ImagePattern imPat;
	private String imgUrl;

	public MapImage(File imgFile) throws IOException {
		super();
		imgUrl = imgFile.getPath();
		imPat = new ImagePattern(SwingFXUtils.toFXImage(ImageIO.read(imgFile), null));
		setFill(imPat);
		setWidth(imPat.getImage().getWidth());
		setHeight(imPat.getImage().getHeight());
	}


	public String getImgUrl() {
		return imgUrl;
	}

	@Override
	public void setFont(String family, boolean bold, boolean italics, int size) {

	}

	@Override
	public Font getFont() {
		return null;
	}

	@Override
	public void drag(double x, double y) {
		double diffX = x - (getX() + (getWidth() / 2));
		double diffY = y - (getY() + (getHeight() / 2));
		double newX = getX() + diffX;
		double newY = getY() + diffY;
		xProperty().set(newX);
		yProperty().set(newY);
	}

	@Override
	public void finishDrag(double x, double y) {

	}

	public void setPosition(double x, double y) {
		setX(x);
		setY(y);
	}

	@Override
	public void select(boolean selecting) {
		if (selecting)
			setEffect(CHOSEN_EFFECT);
		else
			setEffect(null);
	}

	@Override
	public NodeType getType() {
		return NodeType.IMAGE;
	}
}
