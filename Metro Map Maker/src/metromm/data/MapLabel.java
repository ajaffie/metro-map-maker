package metromm.data;

import javafx.geometry.VPos;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import properties_manager.PropertiesManager;

import java.util.NoSuchElementException;

import static metromm.data.MData.CHOSEN_EFFECT;
import static metromm.gui.MLangProps.*;

public class MapLabel extends Text implements MapElement {
	private TextInputDialog textInDialog;
	private Color color;

	public MapLabel(String text, double x, double y, Color color) {
		setTextOrigin(VPos.CENTER);
		setupTextDialog();
		setPosition(x, y);
		setText(text);
		setColor(color);
	}

	public MapLabel(double x, double y, Color color) {
		setTextOrigin(VPos.CENTER);
		setupTextDialog();
		setPosition(x, y);
		showTextDialog();
		setColor(color);
	}

	public void setPosition(double x, double y) {
		setX(x);
		setY(y);
	}

	public boolean isBold() {
		return getFont().getStyle().contains("Bold");
	}

	public boolean isItalic() {
		return getFont().getStyle().contains("Italic");
	}

	@Override
	public void setFont(String family, boolean bold, boolean italics, int size) {
		setFont(Font.font(family, bold ? FontWeight.BOLD : FontWeight.NORMAL, italics ? FontPosture.ITALIC : FontPosture.REGULAR, size));
	}

	@Override
	public void drag(double x, double y) {
		double diffX = x - (getX() + (getLayoutBounds().getWidth() / 2));
		double diffY = y - (getY() + (getLayoutBounds().getHeight() / 2));
		double newX = getX() + diffX;
		double newY = getY() + diffY;
		xProperty().set(newX);
		yProperty().set(newY);
	}

	private void setupTextDialog() {
		PropertiesManager props = PropertiesManager.getPropertiesManager();
		textInDialog = new TextInputDialog();
		textInDialog.getDialogPane().getButtonTypes().clear();
		textInDialog.getDialogPane().getButtonTypes().addAll(
				new ButtonType(props.getProperty(BTN_CONFIRM.toString()), ButtonBar.ButtonData.OK_DONE),
				new ButtonType(props.getProperty(BTN_CANCEL.toString()), ButtonBar.ButtonData.CANCEL_CLOSE)
		);
		textInDialog.setContentText(props.getProperty(TEXT_INPUT_PROMPT.toString()));
		textInDialog.setTitle(props.getProperty(TEXT_INPUT_TITLE.toString()));
		textInDialog.setHeaderText(props.getProperty(TEXT_INPUT_TITLE.toString()));
	}

	public void showTextDialog() {
		String result;
		try {
			result = textInDialog.showAndWait().get();
		} catch (NoSuchElementException e) {
			result = null;
		}
		setText(result != null ? result : getText());
		textInDialog.getEditor().clear();
	}
	@Override
	public void finishDrag(double x, double y) {

	}

	@Override
	public void select(boolean selecting) {
		if (selecting)
			setEffect(CHOSEN_EFFECT);
		else
			setEffect(null);
	}

	@Override
	public NodeType getType() {
		return NodeType.LABEL;
	}

	public void setColor(Color color) {
		this.color = color;
		setFill(color);
	}

	public Color getColor() {
		return color;
	}
}
