package metromm.data;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.paint.Color;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Map {
	private String name;
	private List<Line> lines;
	private List<Station> stations;
	private String backgroundImage;
	private List<MapElement> otherElements;
	private ObservableList<Node> allNodes;
	private Color backgroundColor;
	private double width;

	public Map deepCopy() {
		Map copy = new Map(name);
		copy.width = width;
		copy.height = height;
		copy.backgroundColor = backgroundColor;
		copy.backgroundImage = backgroundImage;
		allNodes.forEach(node -> {
			if (node instanceof MapElement) {
				switch (((MapElement) node).getType()) {

					case LINE_END:
						break;
					case LINE:
						Line line = (Line) node;
						Line l = new Line(line.getName(), line.getColor(), line.start.getX(), line.start.getY(), line.getThickness(), copy, line.getTextColors()[0]);
						l.finishLine(line.end.getX(), line.end.getY(), line.getTextColors()[1]);
						l.setFont(line.getFont());
						l.makeCircular(line.isCircular());
						l.getStations().addAll(line.getStations());
						copy.addLine(l);
						copy.allNodes.addAll(l.getNodes());
						break;
					case LABEL:
						MapLabel oldL = (MapLabel) node;
						MapLabel newL = new MapLabel(oldL.getText(), oldL.getX(), oldL.getY(), oldL.getColor());
						newL.setFont(oldL.getFont());
						copy.addOther(newL);
						copy.allNodes.add(newL);
						break;
					case IMAGE:
						try {
							MapImage im = (MapImage) node;
							MapImage i = new MapImage(new File(im.getImgUrl()));
							i.setPosition(im.getX(), im.getY());
							copy.allNodes.add(i);
							copy.addOther(i);
						} catch (IOException ioe) {
							//no
						}
						break;
					case STATION:
						Station oldS = (Station) node;
						Station s = new Station(oldS.getCenterX(), oldS.getCenterY(), oldS.getRadius(), oldS.getName(), oldS.getColor(), oldS.getTextColor());
						s.setFont(oldS.getFont());
						s.setDirIndex(oldS.getDirIndex());
						copy.addStation(s);
						copy.allNodes.addAll(s, s.getLabel());
						break;
				}
			}
		});
		copy.lines.forEach(line -> {
			line.updateLine();
			line.getStations().forEach(sta -> copy.getStation(sta).addLine(line));
		});
		return copy;
	}

	public double getWidth() {
		return width;
	}

	public double getHeight() {
		return height;
	}

	public void setSize(double w, double h) {
		width = w;
		height = h;
	}

	private double height;

	public Map(String name) {
		this.name = name;
		lines = new ArrayList<>();
		stations = new ArrayList<>();
		otherElements = new ArrayList<>();
		allNodes = FXCollections.observableArrayList();
		width = 1000;
		height = 600;
	}

	public String getName() {
		return name;
	}

	public List<Line> getLines() {
		return lines;
	}

	public ObservableList<Node> getAllNodes() {
		return allNodes;
	}

	public void addStation(Station sta) {
		stations.add(sta);
	}

	public void addLine(Line line) {
		lines.add(line);
	}

	public void addOther(MapElement element) {
		otherElements.add(element);
	}

	public void grow() {
		width += width * .1;
		height += height * .1;
	}

	public void shrink() {
		width *= .9;
		height *= .9;
	}

	public List<Station> getAllStations() {
		return stations;
	}

	public void setBackgroundImage(String imagePath) {
		backgroundImage = imagePath;
	}

	public String getBackgroundImagePath() {
		return backgroundImage;
	}

	public Station getStation(String name) {
		for (Station sta : stations) {
			if (sta.getName().equals(name)) return sta;
		}
		return null;
	}

	public List<MapElement> getOtherElements() {
		return otherElements;
	}

	public Color getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}




	public static class Grid {

	}
}
