package metromm.file;

import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;

import javax.json.*;
import javax.json.stream.JsonGenerator;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class JSONUtils {
	private static final String JSON_RED = "red";
	private static final String JSON_GREEN = "green";
	private static final String JSON_BLUE = "blue";
	private static final String JSON_ALPHA = "alpha";

	public static JsonObject loadJSONFile(String jsonFilePath) throws IOException {
		InputStream is = new FileInputStream(jsonFilePath);
		JsonReader jsonReader = Json.createReader(is);
		JsonObject json = jsonReader.readObject();
		jsonReader.close();
		is.close();
		return json;
	}

	public static boolean writeJson(JsonObject json, String filePath) {
		Map<String, Object> properties = new HashMap<>(1);
		properties.put(JsonGenerator.PRETTY_PRINTING, true);
		JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
		StringWriter sw = new StringWriter();
		JsonWriter jsonWriter = writerFactory.createWriter(sw);
		jsonWriter.writeObject(json);
		jsonWriter.close();
		try (OutputStream os = new FileOutputStream(filePath); PrintWriter pw = new PrintWriter(filePath)) {
			JsonWriter jsonFileWriter = Json.createWriter(os);
			jsonFileWriter.writeObject(json);
			String prettyPrinted = sw.toString();
			pw.write(prettyPrinted);
			return true;
		} catch (IOException ioe) {
			return false;
		}
	}

	public static Color loadColor(JsonObject jsonColor) {
		double red = getDataAsDouble(jsonColor, JSON_RED);
		double green = getDataAsDouble(jsonColor, JSON_GREEN);
		double blue = getDataAsDouble(jsonColor, JSON_BLUE);
		double alpha = getDataAsDouble(jsonColor, JSON_ALPHA);
		Color loadedColor = new Color(red, green, blue, alpha);
		return loadedColor;
	}

	public static JsonObject makeColorJson(Color color) {
		JsonObject colorJson = Json.createObjectBuilder()
				.add(JSON_RED, color.getRed())
				.add(JSON_GREEN, color.getGreen())
				.add(JSON_BLUE, color.getBlue())
				.add(JSON_ALPHA, color.getOpacity()).build();
		return colorJson;
	}

	public static JsonObject makeFontJson(Font font) {
		JsonObjectBuilder tmp = Json.createObjectBuilder()
				.add("family", font.getFamily())
				.add("weight", font.getStyle().contains("bold") ? FontWeight.BOLD.toString() : FontWeight.NORMAL.toString())
				.add("posture", font.getStyle().contains("italic") ? FontPosture.ITALIC.toString() : FontPosture.REGULAR.toString())
				.add("size", font.getSize());
		return tmp.build();
	}

	public static Font loadFont(JsonObject json) {
		return Font.font(json.getString("family"),
				FontWeight.valueOf(json.getString("weight")),
				FontPosture.valueOf(json.getString("posture")),
				getDataAsDouble(json, "size"));
	}

	public static double getDataAsDouble(JsonObject json, String dataName) {
		JsonValue value = json.get(dataName);
		JsonNumber number = (JsonNumber) value;
		return number.bigDecimalValue().doubleValue();
	}
}
