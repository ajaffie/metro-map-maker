package metromm.file;

import properties_manager.PropertiesManager;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;

import static djf.settings.AppStartupConstants.PATH_DATA;
import static metromm.file.JSONProps.JSONProp.*;
import static metromm.file.JSONProps.JSONProp.Type.*;
import static metromm.gui.MLangProps.JSON_PROPS_FILE;

public class JSONProps {
	private static JSONProps instance;
	private HashMap<JSONProp, Number> nums;
	private HashMap<JSONProp, String> strings;
	private boolean debug;
	private List<String> filenames;
	private String filePath;

	public static JSONProps getInstance() {
		if (instance == null) {
			instance = new JSONProps();
			instance.filePath = PATH_DATA + PropertiesManager.getPropertiesManager().getProperty(JSON_PROPS_FILE);
		}
		return instance;
	}

	public boolean loadProps() {
		JsonObject file;
		try {
			file = JSONUtils.loadJSONFile(filePath);
			nums = new HashMap<>();
			strings = new HashMap<>();
			for (JSONProp current : JSONProp.values()) {
				if (file.containsKey(current.toString())) {
					switch (current.type) {
						case NUM:
							nums.put(current, file.getJsonNumber(current.toString()).doubleValue());
							break;
						case STRING:
							strings.put(current, file.getString(current.toString()));
							break;
						case STRING_LIST:
							filenames = new ArrayList<>();
							file.getJsonArray(current.toString()).forEach(js -> filenames.add(js.toString().substring(1, js.toString().length() - 1)));
						default:
							break;
					}
					if (current == DEBUG)
						debug = file.getBoolean(current.toString());
				}
			}
			return true;
		} catch (IOException ioe) {
			file = defaultConfig();
			File newFile = new File(filePath);
			try {
				newFile.createNewFile();
				JSONUtils.writeJson(file, filePath);
			} catch (IOException e) {
				return false;
			}
			return loadProps();
		}
	}

	public double getNum(JSONProp prop) {
		if (prop.type == NUM)
			if (nums.containsKey(prop))
				return nums.get(prop).doubleValue();
			else throw new NoSuchElementException();
		else return 0;
	}

	public void setLatestFile(String name) {
		if (filenames.contains(name))
			filenames.remove(name);
		filenames.add(0, name);
		saveProps();
	}

	public void saveProps() {
		JsonObjectBuilder json = Json.createObjectBuilder();
		nums.forEach((prop, value) -> {
			json.add(prop.toString(), value.doubleValue());
		});
		strings.forEach((prop, value) -> {
			json.add(prop.toString(), value);
		});
		JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
		filenames.forEach(name -> {
			arrayBuilder.add(name);
		});
		json.add(FILES_LIST.toString(), arrayBuilder);
		json.add(DEBUG.toString(), debug);
		JSONUtils.writeJson(json.build(), filePath);
	}

	public List<String> getFilenames() {
		return filenames;
	}

	private JsonObject defaultConfig() {
		JsonObject json = Json.createObjectBuilder()
				.add(STATION_RADIUS.toString(), 10)
				.add(STATION_RADIUS_MIN.toString(), 5)
				.add(STATION_RADIUS_MAX.toString(), 20)
				.add(LINE_THICKNESS.toString(), 5)
				.add(LINE_THICKNESS_MIN.toString(), 2)
				.add(LINE_THICKNESS_MAX.toString(), 20)
				.add(FILES_LIST.toString(), Json.createArrayBuilder().build())
				.add(DEBUG.toString(), true)
				.build();
		return json;
	}

	public boolean isDebug() {
		return debug;
	}

	public enum JSONProp {

		STATION_RADIUS(NUM),
		STATION_RADIUS_MIN(NUM),
		STATION_RADIUS_MAX(NUM),
		LINE_THICKNESS(NUM),
		LINE_THICKNESS_MIN(NUM),
		LINE_THICKNESS_MAX(NUM),
		FILES_LIST(STRING_LIST),
		DEBUG(BOOL);


		private Type type;

		JSONProp(Type type) {
			this.type = type;
		}

		public enum Type {
			NUM, STRING, STATION, LINE, COLOR, STRING_LIST, BOOL
		}
	}
}
