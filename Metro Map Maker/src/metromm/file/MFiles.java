package metromm.file;

import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import djf.ui.AppMessageDialogSingleton;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import metromm.data.*;
import properties_manager.PropertiesManager;

import javax.imageio.ImageIO;
import javax.json.*;
import java.io.File;
import java.io.IOException;

import static djf.settings.AppPropertyType.LOAD_ERROR_TITLE;
import static djf.settings.AppStartupConstants.PATH_WORK;
import static metromm.gui.MLangProps.LOAD_IMAGE_ERROR;

public class MFiles implements AppFileComponent {
	/**
	 * This function must be overridden in the actual component and would
	 * write app data to a file in the necessary format.
	 *
	 * @param data     the data component
	 * @param filePath filepath of save file
	 */
	@Override
	public void saveData(AppDataComponent data, String filePath) {
		Map map = ((MData) data).getMap();
		JSONProps.getInstance().setLatestFile(map.getName());
		JsonObjectBuilder json = Json.createObjectBuilder();
		json.add("name", map.getName());
		if (map.getBackgroundImagePath() != null)
			json.add("background", map.getBackgroundImagePath());
		json.add("w", map.getWidth());
		json.add("h", map.getHeight());
		if (map.getBackgroundColor() != null)
			json.add("backgroundColor", JSONUtils.makeColorJson(map.getBackgroundColor()));
		JsonArrayBuilder allElements = Json.createArrayBuilder();
		map.getAllNodes().forEach(node -> {
			if (node instanceof MapElement) {
				MapElement element = (MapElement) node;
				JsonObjectBuilder current = Json.createObjectBuilder();
				current.add("type", element.getType().toString());
				switch (element.getType()) {

					case LINE_END:
						break;
					case LINE:
						Line line = (Line) element;
						current.add("name", line.getName())
								.add("color", JSONUtils.makeColorJson(line.getColor()))
								.add("thickness", line.getThickness())
								.add("circular", line.isCircular())
								.add("font", JSONUtils.makeFontJson(line.getFont()))
								.add("x1", line.start.getX())
								.add("y1", line.start.getY())
								.add("textColor1", JSONUtils.makeColorJson(line.getTextColors()[0]))
								.add("x2", line.end.getX())
								.add("y2", line.end.getY())
								.add("textColor2", JSONUtils.makeColorJson(line.getTextColors()[1]));
						JsonArrayBuilder starray = Json.createArrayBuilder();
						line.getStations().forEach(starray::add);
						current.add("stations", starray);
						break;
					case LABEL:
						MapLabel label = (MapLabel) element;
						current.add("text", label.getText())
								.add("x", label.getX())
								.add("y", label.getY())
								.add("font", JSONUtils.makeFontJson(label.getFont()))
								.add("color", JSONUtils.makeColorJson(label.getColor()));
						break;
					case IMAGE:
						MapImage image = (MapImage) element;
						current.add("url", image.getImgUrl())
								.add("x", image.getX())
								.add("y", image.getY());
						break;
					case STATION:
						Station station = (Station) element;
						current.add("name", station.getName())
								.add("radius", station.getRadius())
								.add("color", JSONUtils.makeColorJson(station.getColor()))
								.add("x", station.getCenterX())
								.add("y", station.getCenterY())
								.add("dir", station.getDirIndex())
								.add("font", JSONUtils.makeFontJson(station.getFont()))
								.add("textColor", JSONUtils.makeColorJson(station.getTextColor()));
						break;
				}
				allElements.add(current);
			}
		});
		json.add("allElements", allElements);
		JSONUtils.writeJson(json.build(), filePath);

	}

	/**
	 * This function must be overridden in the actual component and would
	 * read app data from a file in the necessary format.
	 *
	 * @param data     the data component of the app
	 * @param filePath The path to the json save file
	 */
	@Override
	public void loadData(AppDataComponent data, String filePath) throws IOException {
		JsonObject file;
		if (filePath.contains(".mmm"))
			file = JSONUtils.loadJSONFile(filePath);
		else
			file = JSONUtils.loadJSONFile(PATH_WORK + filePath + ".mmm");
		Map map = new Map(file.getString("name"));
		if (file.containsKey("background"))
			map.setBackgroundImage(file.getString("background"));
		map.setSize(JSONUtils.getDataAsDouble(file, "w"), JSONUtils.getDataAsDouble(file, "h"));
		if (file.containsKey("backgroundColor"))
			map.setBackgroundColor(JSONUtils.loadColor(file.getJsonObject("backgroundColor")));
		file.getJsonArray("allElements").forEach(jval -> {
			JsonObject obj = (JsonObject) jval;
			switch (MapElement.NodeType.valueOf(obj.getString("type"))) {
				case LINE:
					Line line = new Line(
							obj.getString("name"),
							JSONUtils.loadColor(obj.getJsonObject("color")),
							JSONUtils.getDataAsDouble(obj, "x1"),
							JSONUtils.getDataAsDouble(obj, "y1"),
							JSONUtils.getDataAsDouble(obj, "thickness"),
							map,
							JSONUtils.loadColor(obj.getJsonObject("textColor1")));
					line.finishLine(JSONUtils.getDataAsDouble(obj, "x2"),
							JSONUtils.getDataAsDouble(obj, "y2"),
							JSONUtils.loadColor(obj.getJsonObject("textColor2")));
					line.setFont(JSONUtils.loadFont(obj.getJsonObject("font")));
					line.makeCircular(obj.getBoolean("circular"));
					JsonArray jsonStations = obj.getJsonArray("stations");
					jsonStations.forEach(jsonVal -> line.getStations().add(((JsonString) jsonVal).getString()));
					map.addLine(line);
					map.getAllNodes().addAll(line.getNodes());
					break;
				case LABEL:
					MapLabel label = new MapLabel(
							obj.getString("text"),
							JSONUtils.getDataAsDouble(obj, "x"),
							JSONUtils.getDataAsDouble(obj, "y"),
							JSONUtils.loadColor(obj.getJsonObject("color")));
					label.setFont(JSONUtils.loadFont(obj.getJsonObject("font")));
					map.addOther(label);
					map.getAllNodes().add(label);
					break;
				case IMAGE:
					try {
						MapImage image = new MapImage(new File(obj.getString("url")));
						image.setPosition(
								JSONUtils.getDataAsDouble(obj, "x"),
								JSONUtils.getDataAsDouble(obj, "y"));
						map.addOther(image);
						map.getAllNodes().add(image);
					} catch (IOException ioe) {
						AppMessageDialogSingleton.getSingleton().show(
								PropertiesManager.getPropertiesManager().getProperty(LOAD_ERROR_TITLE.toString()),
								PropertiesManager.getPropertiesManager().getProperty(LOAD_IMAGE_ERROR.toString()) + obj.getString("url"));
					}
					break;
				case STATION:
					Station station = new Station(
							JSONUtils.getDataAsDouble(obj, "x"),
							JSONUtils.getDataAsDouble(obj, "y"),
							JSONUtils.getDataAsDouble(obj, "radius"),
							obj.getString("name"),
							JSONUtils.loadColor(obj.getJsonObject("color")),
							JSONUtils.loadColor(obj.getJsonObject("textColor")));
					station.setFont(JSONUtils.loadFont(obj.getJsonObject("font")));
					station.setDirIndex(obj.getInt("dir"));
					map.addStation(station);
					map.getAllNodes().addAll(station, station.getLabel());
					break;
			}
		});
		map.getLines().forEach(line -> {
			line.updateLine();
			line.getStations().forEach(sta -> {
				map.getStation(sta).addLine(line);
			});
		});
		((MData) data).setMap(map, false);


	}

	/**
	 * Exports the map in 2 files, a .png image and a json formatted as specified
	 *
	 * @param data     the data component of the application
	 * @param filePath output folder
	 */
	@Override
	public void exportData(AppDataComponent data, String filePath) throws IOException {
		//first we'll make the json file.
		Map map = ((MData) data).getMap();
		JsonObjectBuilder json = Json.createObjectBuilder();
		json.add("name", map.getName());
		JsonArrayBuilder linesArray = Json.createArrayBuilder();
		map.getLines().forEach((line) -> {
			JsonObjectBuilder currentLine = Json.createObjectBuilder();
			currentLine.add("name", line.getName());
			currentLine.add("circular", line.isCircular());
			currentLine.add("color", JSONUtils.makeColorJson(line.getColor()));
			JsonArrayBuilder currentStations = Json.createArrayBuilder();
			line.getStations().forEach(currentStations::add);
			currentLine.add("station_names", currentStations);
			linesArray.add(currentLine);
		});
		json.add("lines", linesArray);
		JsonArrayBuilder stationsArray = Json.createArrayBuilder();
		map.getAllStations().forEach((station -> {
			JsonObjectBuilder currentStation = Json.createObjectBuilder();
			currentStation.add("name", station.getName());
			currentStation.add("x", station.getCenterX());
			currentStation.add("y", station.getCenterY());
			stationsArray.add(currentStation);
		}));
		json.add("stations", stationsArray);
		//JSON object is finished, lets get the snapshot.
		WritableImage image = ((MData) data).getApp().getWorkspaceComponent().getCanvas().snapshot(new SnapshotParameters(), null);
		//theres the image, move on to file shenanigans
		if (!JSONUtils.writeJson(json.build(), filePath + "/" + map.getName() + ".json"))
			throw new IOException("Writing JSON failed.");
		File imageFile = new File(filePath + "/" + map.getName() + ".png");
		ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", imageFile);
	}

	/**
	 * Not used in this application.
	 */
	@Override
	public void importData(AppDataComponent data, String filePath) {

	}
}
