package metromm.gui;

import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import metromm.MetroApp;
import metromm.data.*;
import properties_manager.PropertiesManager;

import java.io.File;
import java.io.IOException;

import static djf.settings.AppPropertyType.*;
import static djf.settings.AppStartupConstants.*;
import static metromm.gui.MLangProps.*;

public class MapController {
	private MetroApp app;
	private PropertiesManager props;
	private Line newLine;
	private String newStationName;
	private MapImage newImage;

	MapController(MetroApp app) {
		this.app = app;
		props = PropertiesManager.getPropertiesManager();
	}

	public void handleCreateMap() {
		String selectedFilename;
		PropertiesManager props = PropertiesManager.getPropertiesManager();
		while (true) {
			String filename = MetroApp.showTextInputDialog(app.getGUI().getWindow(), NEW_MAP_TITLE, NEW_MAP_HEADER, NEW_MAP_PROMPT);
			if (filename == null)
				return;
			File file = new File(PATH_WORK + filename + ".mmm");
			if (filename.equals("")) {
				AppMessageDialogSingleton.getSingleton().show(props.getProperty(APP_TITLE.toString()), props.getProperty(NEW_UNNAMED_ENTERED.toString()));
			} else if (!file.exists()) {
				selectedFilename = filename;
				app.getGUI().getFileController().setCurrentWorkFile(file);
				break;
			} else {
				AppMessageDialogSingleton.getSingleton().show(props.getProperty(APP_TITLE.toString()), props.getProperty(MAP_EXISTS_MSG.toString()));
			}
		}
		app.getDataComponent().newMap(selectedFilename);
		app.getWorkspaceComponent().resetWorkspace();
		app.getWorkspaceComponent().reloadWorkspace(app.getDataComponent());
		app.getWorkspaceComponent().activateWorkspace(app.getGUI().getAppPane());
		try {
			app.getFileComponent().saveData(app.getDataComponent(), selectedFilename);
			AppMessageDialogSingleton.getSingleton().show(props.getProperty(NEW_COMPLETED_TITLE), props.getProperty(NEW_COMPLETED_MESSAGE));
		} catch (IOException e) {
			AppMessageDialogSingleton.getSingleton().show(props.getProperty(SAVE_ERROR_TITLE), props.getProperty(SAVE_ERROR_MESSAGE));
		}

	}

	/**
	 * This method handles exporting the map to the export directory based on the name of the map.
	 */
	@SuppressWarnings({"ResultOfMethodCallIgnored"})
	public void handleExportMap() {
		PropertiesManager props = PropertiesManager.getPropertiesManager();
		try {
			//EXPORT MAP
			File exportDir = new File(PATH_EXPORT + app.getDataComponent().getMap().getName());
			if (!exportDir.exists()) {
				exportDir.mkdirs();
			} else {
				exportDir.delete();
				exportDir.mkdirs();
			}
			app.getFileComponent().exportData(app.getDataComponent(), exportDir.getPath());
			AppMessageDialogSingleton.getSingleton().show(props.getProperty(EXPORT_TITLE.toString()), props.getProperty(EXPORT_SUCCESS_MESSAGE.toString()));
		} catch (Exception e) {
			AppMessageDialogSingleton.getSingleton().show(props.getProperty(EXPORT_TITLE.toString()), props.getProperty(EXPORT_FAIL_MESSAGE.toString()));
		}
	}

	public void handleChangeBackground() {
		FileChooser imageChooser = new FileChooser();
		imageChooser.setInitialDirectory(new File(PATH_IMAGES));
		imageChooser.setTitle(props.getProperty(NEW_BACKGROUND_TITLE.toString()));
		FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("Image files (*.jpg, *.png)", "*.JPG", "*.PNG", "*.JPEG");
		imageChooser.getExtensionFilters().add(extensionFilter);
		File newImageFile = imageChooser.showOpenDialog(app.getGUI().getWindow());
		if (app.getWorkspaceComponent().setBackgroundImage(newImageFile))
			//set image URL in map after it is successfully loaded.
			app.getDataComponent().getMap().setBackgroundImage(newImageFile.getPath());
		else
			AppMessageDialogSingleton.getSingleton().show(props.getProperty(NEW_BACKGROUND_TITLE.toString()), props.getProperty(NEW_BACKGROUND_FAIL.toString()));
	}

	public void handleAddImage() {
		FileChooser imageChooser = new FileChooser();
		imageChooser.setInitialDirectory(new File(PATH_IMAGES));
		imageChooser.setTitle(props.getProperty(NEW_IMAGE_TITLE.toString()));
		FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("Image files (*.jpg, *.png)", "*.JPG", "*.PNG", "*.JPEG");
		imageChooser.getExtensionFilters().add(extensionFilter);
		File newImageFile = imageChooser.showOpenDialog(app.getGUI().getWindow());
		try {
			newImage = new MapImage(newImageFile);
			app.getDataComponent().setState(MData.MState.ADD_IMAGE);
			app.getWorkspaceComponent().enableButtons();
		} catch (Exception e) {
			AppMessageDialogSingleton.getSingleton().show(props.getProperty(NEW_IMAGE_TITLE.toString()), props.getProperty(NEW_IMAGE_ERROR.toString()));
		}
	}


	public void handleMouseClicked(MouseEvent me) {
		MData data = app.getDataComponent();
		switch (data.getState()) {

			case SELECTING:
			case DRAGGING:
			case DRAGGING_NOTHING:
				MapElement selected = data.selectTopElement(me.getX(), me.getY());
				if (selected != null) {
					data.setState(MData.MState.DRAGGING);
				} else {
					data.setState(MData.MState.DRAGGING_NOTHING);
				}
				break;
			case ADD_LINE:
				data.snap();
				AddLineSingleton als = AddLineSingleton.getInstance();
				newLine = new Line(als.getChosenName(), als.getChosenColor(), me.getX(), me.getY(), app.getWorkspaceComponent().getCurrentLineThickness(), data.getMap(), app.getWorkspaceComponent().getCurrentFontColor());
				data.addNewLine(newLine);
				data.setState(MData.MState.FINISH_LINE);
				break;
			case FINISH_LINE:
				newLine.finishLine(me.getX(), me.getY(), app.getWorkspaceComponent().getCurrentFontColor());
				data.finishNewLine(newLine);
				data.select(newLine);
				newLine = null;
				data.setState(MData.MState.DRAGGING);
				break;
			case ADD_STATION:
				data.snap();
				Station newStation = new Station(me.getX(), me.getY(), app.getWorkspaceComponent().getCurrentStationRadius(), newStationName, app.getWorkspaceComponent().getCurrentStationColor(), app.getWorkspaceComponent().getCurrentFontColor());
				newStationName = null;
				app.getDataComponent().addStation(newStation);
				app.getDataComponent().select(newStation);
				data.setState(MData.MState.DRAGGING);
				break;
			case ADD_IMAGE:
				newImage.setPosition(me.getX(), me.getY());
				data.addOther(newImage);
				data.select(newImage);
				data.setState(MData.MState.DRAGGING);
				newImage = null;
				break;
			case ADD_LABEL:
				data.snap();
				MapLabel newLabel = new MapLabel(me.getX(), me.getY(), app.getWorkspaceComponent().getCurrentFontColor());
				newLabel.setFont(app.getWorkspaceComponent().getCurrentFont());
				data.addOther(newLabel);
				data.select(newLabel);
				data.setState(MData.MState.DRAGGING);
				break;
			case ADD_STA_TO_LINE:
				data.deselect();
				MapElement clicked = data.getTopElement(me.getX(), me.getY());
				if (clicked == null) {
					data.setState(MData.MState.DRAGGING_NOTHING);
					break;
				}
				Line selectedLine = data.selectLine(app.getWorkspaceComponent().linesCombo.getSelectionModel().getSelectedItem());
				if (clicked instanceof Station) {
					data.snap();
					((Station) clicked).addToLine(selectedLine.getClosestStation(((Station) clicked).getCenterX(), ((Station) clicked).getCenterY()), selectedLine);
					data.setState(MData.MState.ADD_STA_TO_LINE);
				} else {
					data.setState(MData.MState.DRAGGING);
					data.select(clicked);
				}
				break;
			case REM_STA_FROM_LINE:
				data.deselect();
				MapElement clickedRem = data.getTopElement(me.getX(), me.getY());
				if (clickedRem == null) {
					data.setState(MData.MState.DRAGGING_NOTHING);
					break;
				}
				Line selectedLineRem = data.selectLine(app.getWorkspaceComponent().linesCombo.getSelectionModel().getSelectedItem());
				if (clickedRem instanceof Station) {
					data.snap();
					((Station) clickedRem).removeLine(selectedLineRem);
					selectedLineRem.getStations().remove(((Station) clickedRem).getName());
					selectedLineRem.updateLine();
					data.setState(MData.MState.REM_STA_FROM_LINE);
				} else {
					data.setState(MData.MState.DRAGGING);
					data.select(clickedRem);
				}
				break;
		}
		app.getWorkspaceComponent().enableButtons();
	}

	public void handleDoubleClick(MouseEvent me) {
		MData data = app.getDataComponent();
		if (data.getState().equals(MData.MState.SELECTING))
			data.selectTopElement(me.getX(), me.getY());
		MapElement element = data.getSelected();
		if (element instanceof MapLabel) {
			data.snap();
			((MapLabel) element).showTextDialog();
			app.getWorkspaceComponent().enableButtons();
		} else {
			handleMouseClicked(me);
		}

	}

	public void handleAddLabel() {
		app.getDataComponent().setState(MData.MState.ADD_LABEL);
		app.getWorkspaceComponent().enableButtons();
	}

	public void handleAddLine() {
		AddLineSingleton.getInstance().showAndWait();
		if (AddLineSingleton.getInstance().canCreate())
			app.getDataComponent().setState(MData.MState.ADD_LINE);
		app.getWorkspaceComponent().enableButtons();
	}

	/**
	 * Precondition: line is selected
	 */
	public void handleRemoveLine() {
		Line line = (Line) app.getDataComponent().getSelected();
		AppYesNoCancelDialogSingleton ynDialog = AppYesNoCancelDialogSingleton.getSingleton();
		ynDialog.show(props.getProperty(REMOVE_LINE_TITLE.toString()), props.getProperty(REMOVE_LINE_MSG.toString()));
		switch (ynDialog.getSelection()) {
			case AppYesNoCancelDialogSingleton.YES:
				app.getDataComponent().getMap().getLines().remove(line);
				app.getWorkspaceComponent().loadSettings(line);
				app.getDataComponent().deselect();
				app.getWorkspaceComponent().getCanvas().getChildren().removeAll(line.getNodes());

				break;
			default:
		}
		app.getWorkspaceComponent().enableButtons();
	}

	public void handleRemoveStation() {
		Station station = (Station) app.getDataComponent().getSelected();
		AppYesNoCancelDialogSingleton ynDialog = AppYesNoCancelDialogSingleton.getSingleton();
		ynDialog.show(props.getProperty(REMOVE_STATION_TITLE.toString()), props.getProperty(REMOVE_STATION_MSG.toString()));
		switch (ynDialog.getSelection()) {
			case AppYesNoCancelDialogSingleton.YES:
				app.getDataComponent().removeStation(station);
				app.getDataComponent().deselect();
				break;
			default:
		}
		app.getWorkspaceComponent().enableButtons();
	}

	/**
	 * Precondition: line is selected.
	 */
	public void handleEditLine() {
		Line line = (Line) app.getDataComponent().getSelected();
		EditLineSingleton.getInstance().populate(line.getName(), line.getColor(), line.isCircular());
		EditLineSingleton.getInstance().showAndWait();
		line.setName(EditLineSingleton.getInstance().getChosenName());
		line.setColor(EditLineSingleton.getInstance().getChosenColor());
		line.makeCircular(EditLineSingleton.getInstance().isCircular());
		app.getWorkspaceComponent().loadSettings(line);
		app.getWorkspaceComponent().enableButtons();
	}

	public void handleMouseDragged(MouseEvent me) {
		if (app.getDataComponent().getState() == MData.MState.DRAGGING && app.getDataComponent().getSelected() != null) {
			app.getDataComponent().getSelected().drag(me.getX(), me.getY());
		}
		app.getWorkspaceComponent().enableButtons();
	}

	public void handleMouseDragReleased(MouseEvent me) {
		MData data = app.getDataComponent();
		switch (data.getState()) {

			case SELECTING:
				break;
			case DRAGGING:
				data.getSelected().finishDrag(me.getX(), me.getY());
				data.setState(MData.MState.SELECTING);
				break;
			case DRAGGING_NOTHING:
				data.setState(MData.MState.SELECTING);
				break;
			case ADD_LINE:
				break;
			case FINISH_LINE:
				break;
			case ADD_STATION:
				break;
		}
		app.getWorkspaceComponent().enableButtons();
	}

	/**
	 * Precondition: a line is selected
	 */
	public void handleChangeLineThickness() {
		Line line = (Line) app.getDataComponent().getSelected();
		line.setThickness(app.getWorkspaceComponent().getCurrentLineThickness());
		app.getWorkspaceComponent().enableButtons();
	}

	public void handleAddStation() {
		newStationName = MetroApp.showTextInputDialog(app.getGUI().getWindow(),
				ADD_STATION_TITLE, ADD_STATION_HEADER, ADD_STATION_MSG);
		if (newStationName == null) return;
		app.getDataComponent().setState(MData.MState.ADD_STATION);
		app.getWorkspaceComponent().enableButtons();
	}

	/**
	 * Precondition: a line is selected
	 */
	public void handleChangeStationRadius() {
		Station sta = (Station) app.getDataComponent().getSelected();
		sta.setRadius(app.getWorkspaceComponent().getCurrentStationRadius());
		sta.updateLabelPos();
	}


	public void handleChangeStationColor() {
		((Station) app.getDataComponent().getSelected()).setColor(app.getWorkspaceComponent().getCurrentStationColor());
	}

	public void handleSaveAsRequest() {
		app.getGUI().getFileController().setCurrentWorkFile(null);
		app.getGUI().getFileController().handleSaveRequest();

	}

	public void handleChangeFont() {
		MapElement element = app.getDataComponent().getSelected();
		if (element != null) {
			Font newFont = app.getWorkspaceComponent().getCurrentFont();
			Color fontColor = app.getWorkspaceComponent().getCurrentFontColor();
			switch (element.getType()) {
				case LINE_END:
					((LineEnd) element).setTextColor(fontColor);
					((LineEnd) element).setFont(newFont);
					break;
				case LABEL:
					((MapLabel) element).setFont(newFont);
					((MapLabel) element).setColor(fontColor);
					break;
				case STATION:
					((Station) element).setTextColor(fontColor);
					((Station) element).setFont(newFont);
					break;
			}
		}
	}

	public void handleGrow() {
		app.getDataComponent().getMap().grow();
		app.getWorkspaceComponent().setMapSize(app.getDataComponent().getMap().getWidth(), app.getDataComponent().getMap().getHeight());
	}

	public void handleShrink() {
		app.getDataComponent().getMap().shrink();
		app.getWorkspaceComponent().setMapSize(app.getDataComponent().getMap().getWidth(), app.getDataComponent().getMap().getHeight());
	}

	public void handleRemElement() {
		app.getDataComponent().removeElement(app.getDataComponent().getSelected());
	}

	public void handleAddStaToLine() {
		app.getDataComponent().deselect();
		app.getDataComponent().selectLine(app.getWorkspaceComponent().linesCombo.getSelectionModel().getSelectedItem());
		app.getDataComponent().setState(MData.MState.ADD_STA_TO_LINE);
		app.getWorkspaceComponent().enableButtons();
	}

	public void handleRemStaFromLine() {
		app.getDataComponent().deselect();
		app.getDataComponent().selectLine(app.getWorkspaceComponent().linesCombo.getSelectionModel().getSelectedItem());
		app.getDataComponent().setState(MData.MState.REM_STA_FROM_LINE);
		app.getWorkspaceComponent().enableButtons();
	}
}
