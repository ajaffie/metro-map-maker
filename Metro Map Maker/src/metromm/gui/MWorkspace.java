package metromm.gui;

import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import djf.ui.AppGUI;
import djf.ui.AppMessageDialogSingleton;
import javafx.beans.InvalidationListener;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import metromm.MetroApp;
import metromm.data.*;
import metromm.file.JSONProps;
import properties_manager.PropertiesManager;

import javax.imageio.ImageIO;
import java.io.File;
import java.util.List;

import static djf.settings.AppPropertyType.*;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import static javafx.scene.layout.BackgroundSize.AUTO;
import static metromm.file.JSONProps.JSONProp.*;
import static metromm.gui.MLangProps.*;
import static metromm.gui.MStyle.*;

public class MWorkspace extends AppWorkspaceComponent {
	private MetroApp app;
	private AppGUI gui;
	private JSONProps jsonProps;


	//top toolbar
	Button btSaveAs, btExport, btUndo, btRedo, btAbout;
	FlowPane undoRedoToolbar, aboutToolbar;
	//Edit toolbar
	VBox leftPane;
	//lines row
	VBox linesToolbar;
	HBox linesUpper;
	Label linesLabel;
	ComboBox<String> linesCombo;
	Button btEditLine;
	HBox linesMid;
	Button btAddLine, btRemoveLine, btAddStatToLine, btRemStatFromLine, btListStationsInLine;
	Slider lineThicknessSlider;
	//Stations row
	VBox stationsToolbar;
	HBox stationsUpper;
	Label stationsLabel;
	ComboBox<String> stationsCombo;
	ColorPicker stationsColorPicker;
	HBox stationsMid;
	Button btAddStation, btRemStation, btSnapStation, btMoveStatLabel, btRotateStatLabel;
	Slider stationRadiusSlider;
	//station router row
	HBox stationRouterToolbar;
	VBox statRoutLeft;
	ComboBox<String> routFromCombo, routToCombo;
	Button btRoute;
	//decor row
	VBox decorToolbar;
	HBox decorUpper;
	Label decorLabel;
	ColorPicker backgroundColorPicker;
	HBox decorLower;
	Button btSetBackgroundImage, btAddImage, btAddLabel, btRemElement;
	//font row
	VBox fontToolbar;
	HBox fontUpper;
	Label fontLabel;
	ColorPicker fontColorPicker;
	HBox fontLower;
	ToggleButton btBold, btItalics;
	ComboBox<Integer> fontSizeCombo;
	ComboBox<String> fontCombo;
	//nav row
	VBox navToolbar;
	HBox navUpper;
	Label navLabel;
	CheckBox checkShowGrid;
	HBox navLower;
	Button btZoomIn, btZoomOut, btBigger, btSmaller;
	Pane canvas;
	ZoomPane zoomPane;

	private PropertiesManager props;
	private MapController mapController;

	public MWorkspace(MetroApp app) {
		this.app = app;
		mapController = new MapController(app);
		gui = app.getGUI();
		jsonProps = JSONProps.getInstance();
		props = PropertiesManager.getPropertiesManager();
		initLayout();
		initControllers();
		initStyle();

		initToolbarControls();

	}

	private void initToolbarControls() {
		//Font controls
		fontCombo.getSelectionModel().select("Arial");
		fontSizeCombo.getSelectionModel().select(new Integer(12));
		btBold.setSelected(false);
		btItalics.setSelected(false);
		fontColorPicker.setValue(Color.BLACK);


	}

	public void setMapSize(double w, double h) {
		canvas.setMinHeight(h);
		canvas.setMaxHeight(h);
		canvas.setMinWidth(w);
		canvas.setMaxWidth(w);
		canvas.setPrefWidth(w);
		canvas.setPrefHeight(h);
	}
	private void initLayout() {
		workspace = new BorderPane();
		//canvas
		canvas = new Pane();

		zoomPane = new ZoomPane(canvas);
		zoomPane.setMaxHeight(Double.MAX_VALUE);
		zoomPane.setMaxWidth(Double.MAX_VALUE);
		canvas.setMaxHeight(Double.MAX_VALUE);
		canvas.setMaxWidth(Double.MAX_VALUE);

		((BorderPane) workspace).setCenter(zoomPane);
		//TOP TOOLBAR
		btSaveAs = gui.initChildButton(gui.getFileToolbar(), SAVE_AS_ICON.toString(), SAVE_AS_TOOLTIP.toString(), false);
		btExport = gui.initChildButton(gui.getFileToolbar(), EXPORT_ICON.toString(), EXPORT_TOOLTIP.toString(), false);
		undoRedoToolbar = new FlowPane();
		btUndo = gui.initChildButton(undoRedoToolbar, UNDO_ICON.toString(), UNDO_TOOLTIP.toString(), true);
		btRedo = gui.initChildButton(undoRedoToolbar, REDO_ICON.toString(), REDO_TOOLTIP.toString(), true);
		gui.getTopToolbarPane().getChildren().add(undoRedoToolbar);
		aboutToolbar = new FlowPane();
		btAbout = gui.initChildButton(aboutToolbar, ABOUT_ICON.toString(), ABOUT_TOOLTIP.toString(), false);
		if (JSONProps.getInstance().isDebug()) {
			Label debugState = new Label();
			debugState.textProperty().bind(app.getDataComponent().stateProperty().asString());
			aboutToolbar.getChildren().add(debugState);
		}

		gui.getTopToolbarPane().getChildren().add(aboutToolbar);
		//EDIT TOOLBAR
		leftPane = new VBox();
		//LINES ROW
		linesToolbar = new VBox();
		linesUpper = new HBox();
		linesLabel = new Label(props.getProperty(LINES_LABEL));
		linesCombo = new ComboBox<>();
		HBox.setHgrow(linesCombo, Priority.ALWAYS);
		linesUpper.getChildren().addAll(linesLabel, hSpacer(Priority.SOMETIMES), linesCombo, hSpacer(Priority.SOMETIMES));
		HBox.setHgrow(linesCombo, Priority.ALWAYS);
		btEditLine = gui.initChildButton(linesUpper, EDIT_LINE_ICON.toString(), EDIT_LINE_TOOLTIP.toString(), true);
		linesMid = new HBox();
		btAddLine = gui.initChildButton(linesMid, ADD_LINE_ICON.toString(), ADD_LINE_TOOLTIP.toString(), false);
		btRemoveLine = gui.initChildButton(linesMid, REMOVE_LINE_ICON.toString(), REMOVE_LINE_TOOLTIP.toString(), true);
		btAddStatToLine = new Button(props.getProperty(ADD_STAT_TO_LINE.toString()));
		btAddStatToLine.setTooltip(new Tooltip(props.getProperty(ADD_STAT_TO_LINE_TOOLTIP.toString())));
		btAddStatToLine.setDisable(true);
		btRemStatFromLine = new Button(props.getProperty(REM_STAT_LINE));
		btRemStatFromLine.setTooltip(new Tooltip(props.getProperty(REM_STAT_LINE_TOOLTIP.toString())));
		btRemStatFromLine.setDisable(true);
		linesMid.getChildren().addAll(btAddStatToLine, btRemStatFromLine);
		btListStationsInLine = gui.initChildButton(linesMid, LIST_STATIONS_ICON.toString(), LIST_STATIONS_TOOLTIP.toString(), true);
		lineThicknessSlider = new Slider(jsonProps.getNum(LINE_THICKNESS_MIN), jsonProps.getNum(LINE_THICKNESS_MAX), jsonProps.getNum(LINE_THICKNESS));
		lineThicknessSlider.setTooltip(new Tooltip(props.getProperty(LINE_THICK_SLIDER_TOOLTIP.toString())));
		linesToolbar.getChildren().addAll(linesUpper, linesMid, lineThicknessSlider);
		//STATIONS ROW
		stationsToolbar = new VBox();
		stationsUpper = new HBox();
		stationsLabel = new Label(props.getProperty(STATIONS_LABEL.toString()));
		stationsCombo = new ComboBox<>();
		HBox.setHgrow(stationsCombo, Priority.ALWAYS);
		stationsColorPicker = new ColorPicker(Color.BLACK);
		stationsColorPicker.setTooltip(new Tooltip(props.getProperty(STATIONS_COLOR_TOOLTIP.toString())));
		stationsUpper.getChildren().addAll(stationsLabel, stationsCombo, stationsColorPicker);
		stationsMid = new HBox();
		stationsMid.setAlignment(Pos.CENTER);
		btAddStation = gui.initChildButton(stationsMid, ADD_STATION_ICON.toString(), ADD_STATION_TOOLTIP.toString(), false);
		btRemStation = gui.initChildButton(stationsMid, REM_STATION_ICON.toString(), REM_STATION_TOOLTIP.toString(), true);
		btSnapStation = new Button(props.getProperty(STAT_SNAP.toString()));
		btSnapStation.setTooltip(new Tooltip(props.getProperty(STAT_SNAP_TOOLTIP.toString())));
		btSnapStation.setDisable(true);
		btMoveStatLabel = new Button(props.getProperty(STAT_MOVE_LABEL.toString()));
		btMoveStatLabel.setTooltip(new Tooltip(props.getProperty(STAT_MOVE_LABEL_TOOLTIP.toString())));
		btMoveStatLabel.setDisable(true);
		btRotateStatLabel = new Button(props.getProperty(STAT_ROT_LABEL.toString()));
		btRotateStatLabel.setTooltip(new Tooltip(props.getProperty(STAT_ROT_LABEL_TOOLTIP.toString())));
		btRotateStatLabel.setDisable(true);
		stationsMid.getChildren().addAll(btSnapStation, btMoveStatLabel, btRotateStatLabel);
		stationRadiusSlider = new Slider(jsonProps.getNum(STATION_RADIUS_MIN), jsonProps.getNum(STATION_RADIUS_MAX), jsonProps.getNum(STATION_RADIUS));
		stationRadiusSlider.setTooltip(new Tooltip(props.getProperty(STAT_RADIUS_TOOLTIP.toString())));
		stationsToolbar.getChildren().addAll(stationsUpper, stationsMid, stationRadiusSlider);
		stationRouterToolbar = new HBox();
		stationRouterToolbar.setAlignment(Pos.CENTER);
		statRoutLeft = new VBox();
		routFromCombo = new ComboBox<>();
		routFromCombo.setTooltip(new Tooltip(props.getProperty(FROM_COMBO_TOOLTIP.toString())));
		routToCombo = new ComboBox<>();
		routToCombo.setTooltip(new Tooltip(props.getProperty(TO_COMBO_TOOLTIP.toString())));
		statRoutLeft.getChildren().addAll(routFromCombo, routToCombo);
		HBox.setHgrow(statRoutLeft, Priority.ALWAYS);
		stationRouterToolbar.getChildren().add(statRoutLeft);
		btRoute = gui.initChildButton(stationRouterToolbar, ROUTE_ICON.toString(), ROUTE_TOOLTIP.toString(), true);
		//DECOR ROW
		decorToolbar = new VBox();
		decorUpper = new HBox();
		decorLabel = new Label(props.getProperty(DECOR_LABEL.toString()));
		backgroundColorPicker = new ColorPicker(Color.WHITE);
		backgroundColorPicker.setTooltip(new Tooltip(props.getProperty(DECOR_COLOR_TOOLTIP.toString())));
		decorUpper.getChildren().addAll(decorLabel, hSpacer(Priority.ALWAYS), backgroundColorPicker);
		decorLower = new HBox();
		btSetBackgroundImage = new Button(props.getProperty(SET_BK_IMG_LABEL.toString()));
		btSetBackgroundImage.setTooltip(new Tooltip(props.getProperty(SET_BK_IMG_TOOLTIP.toString())));
		decorLower.getChildren().add(btSetBackgroundImage);
		decorLower.getChildren().add(hSpacer(Priority.ALWAYS));
		btAddImage = gui.initChildButton(decorLower, ADD_IMAGE_ICON.toString(), ADD_IMAGE_TOOLTIP.toString(), false);
		decorLower.getChildren().add(hSpacer(Priority.ALWAYS));
		btAddLabel = gui.initChildButton(decorLower, ADD_TEXT_ICON.toString(), ADD_TEXT_TOOLTIP.toString(), false);
		decorLower.getChildren().add(hSpacer(Priority.ALWAYS));
		btRemElement = gui.initChildButton(decorLower, REMOVE_ICON.toString(), REM_DECOR_TOOLTIP.toString(), true);
		decorToolbar.getChildren().addAll(decorUpper, decorLower);
		//FONT ROW
		fontToolbar = new VBox();
		fontUpper = new HBox();
		fontLabel = new Label(props.getProperty(FONT_LABEL.toString()));
		fontColorPicker = new ColorPicker(Color.BLACK);
		fontUpper.getChildren().addAll(fontLabel, hSpacer(Priority.ALWAYS), fontColorPicker);
		fontLower = new HBox();
		btBold = new ToggleButton();
		btBold.setGraphic(new ImageView(new Image(FILE_PROTOCOL + PATH_IMAGES + props.getProperty(BOLD_ICON.toString()))));
		btBold.setTooltip(new Tooltip(props.getProperty(BOLD_TOOLTIP.toString())));
		btItalics = new ToggleButton();
		btItalics.setGraphic(new ImageView(new Image(FILE_PROTOCOL + PATH_IMAGES + props.getProperty(ITALICS_ICON.toString()))));
		btItalics.setTooltip(new Tooltip(props.getProperty(ITALICS_TOOLTIP.toString())));
		fontSizeCombo = new ComboBox<>();
		props.getPropertyOptionsList(FONT_SIZE_COMBO_BOX_OPTIONS.toString()).forEach(str -> fontSizeCombo.getItems().add(Integer.parseInt(str)));
		fontSizeCombo.setTooltip(new Tooltip(props.getProperty(FONT_SIZE_TOOLTIP.toString())));
		fontCombo = new ComboBox<>();
		fontCombo.getItems().addAll(props.getPropertyOptionsList(FONT_FAMILY_COMBO_BOX_OPTIONS.toString()));
		fontCombo.setTooltip(new Tooltip(props.getProperty(FONT_COMBO_TOOLTIP.toString())));
		HBox.setHgrow(fontCombo, Priority.ALWAYS);
		fontLower.getChildren().addAll(hSpacer(Priority.ALWAYS), btBold, hSpacer(Priority.ALWAYS), btItalics, hSpacer(Priority.ALWAYS), fontSizeCombo, hSpacer(Priority.ALWAYS), fontCombo, hSpacer(Priority.ALWAYS));
		fontToolbar.getChildren().addAll(fontUpper, fontLower);
		//NAV ROW
		navToolbar = new VBox();
		navUpper = new HBox();
		navLabel = new Label(props.getProperty(NAV_LABEL.toString()));
		checkShowGrid = new CheckBox(props.getProperty(SHOW_GRID_LABEL.toString()));
		checkShowGrid.setTooltip(new Tooltip(props.getProperty(SHOW_GRID_TOOLTIP)));
		navUpper.getChildren().addAll(navLabel, hSpacer(Priority.ALWAYS), checkShowGrid);
		navLower = new HBox();
		btZoomIn = gui.initChildButton(navLower, ZOOM_IN_ICON.toString(), ZOOM_IN_TOOLTIP.toString(), false);
		navLower.getChildren().add(hSpacer(Priority.ALWAYS));
		btZoomOut = gui.initChildButton(navLower, ZOOM_OUT_ICON.toString(), ZOOM_OUT_TOOLTIP.toString(), false);
		navLower.getChildren().add(hSpacer(Priority.ALWAYS));
		btBigger = gui.initChildButton(navLower, BIGGER_ICON.toString(), BIGGER_TOOLTIP.toString(), false);
		navLower.getChildren().add(hSpacer(Priority.ALWAYS));
		btSmaller = gui.initChildButton(navLower, SMALLER_ICON.toString(), SMALLER_TOOLTIP.toString(), false);
		navToolbar.getChildren().addAll(navUpper, navLower);
		//assemble edit toolbar
		leftPane.getChildren().addAll(linesToolbar, stationsToolbar, stationRouterToolbar, decorToolbar, fontToolbar, navToolbar);
		leftPane.maxWidthProperty().bind(app.getGUI().getWindow().widthProperty().divide(5));
		//assemble workspace
		((BorderPane) workspace).setLeft(leftPane);
		((BorderPane) workspace).setTop(app.getGUI().getTopToolbarPane());
	}

	private void initControllers() {


		((Button) gui.getFileToolbar().getChildren().get(0)).setOnAction(e -> mapController.handleCreateMap());
		btSaveAs.setOnAction(e -> mapController.handleSaveAsRequest());
		btExport.setOnAction(e -> mapController.handleExportMap());
		btAbout.setOnAction(e -> AppMessageDialogSingleton.getSingleton().show(props.getProperty(ABOUT_TITLE.toString()), props.getProperty(ABOUT_CONTENT.toString()).replace("\\n", "\n")));
		gui.getWindow().setOnCloseRequest(e -> {app.getGUI().getFileController().handleExitRequest();});
		btSetBackgroundImage.setOnAction(e -> {
			app.getDataComponent().snap();
			mapController.handleChangeBackground();
		});
		btAddLine.setOnAction(e -> {
			mapController.handleAddLine();
		});
		btEditLine.setOnAction(e -> {
			app.getDataComponent().selectLine(linesCombo.getValue());
			mapController.handleEditLine();
		});
		btRemoveLine.setOnAction(e -> {
			app.getDataComponent().selectLine(linesCombo.getValue());
			mapController.handleRemoveLine();
			if (!linesCombo.getItems().isEmpty())
				linesCombo.getSelectionModel().selectFirst();
		});
		btAddStation.setOnAction(e -> {
			mapController.handleAddStation();
		});
		btRemStation.setOnAction(e -> {
			app.getDataComponent().selectStation(stationsCombo.getValue());
			mapController.handleRemoveStation();
			if (!stationsCombo.getItems().isEmpty())
				stationsCombo.getSelectionModel().selectFirst();
		});
		btMoveStatLabel.setOnAction(e -> {
			app.getDataComponent().snap();
			app.getDataComponent().selectStation(stationsCombo.getValue());
			((Station) app.getDataComponent().getSelected()).toggleDirection();
		});

		linesCombo.getSelectionModel().selectedItemProperty().addListener((name) -> {
			app.getDataComponent().selectLine(((ReadOnlyObjectProperty<String>) name).getValue());
		});
		stationsCombo.getSelectionModel().selectedItemProperty().addListener((name) -> {
			app.getDataComponent().selectStation(((ReadOnlyObjectProperty<String>) name).getValue());
		});
		lineThicknessSlider.valueChangingProperty().addListener((observable, wasChanging, changing) -> {
			if (linesCombo.getItems().isEmpty()) return;
			if (!changing && wasChanging) {
				app.getDataComponent().snap();
				double tmpThickness = getCurrentLineThickness();
				app.getDataComponent().selectLine(linesCombo.getValue());
				lineThicknessSlider.setValue(tmpThickness);
				mapController.handleChangeLineThickness();
			}
		});
		stationRadiusSlider.valueChangingProperty().addListener((observable, wasChanging, changing) -> {
			if (stationsCombo.getItems().isEmpty()) return;
			if (!changing && wasChanging) {
				app.getDataComponent().snap();
				double tmpThickness = getCurrentStationRadius();
				app.getDataComponent().selectStation(stationsCombo.getValue());
				stationRadiusSlider.setValue(tmpThickness);
				mapController.handleChangeStationRadius();
			}
		});
		stationsColorPicker.valueProperty().addListener(e -> {
			if (!stationsCombo.getItems().isEmpty()) {
				app.getDataComponent().snap();
				app.getDataComponent().selectStation(stationsCombo.getValue());
				mapController.handleChangeStationColor();
			}
		});

		canvas.setOnMouseClicked(me -> {
			if (me.getClickCount() > 1)
				mapController.handleDoubleClick(me);
			else
				mapController.handleMouseClicked(me);
		});
		canvas.setOnMouseDragged(me -> mapController.handleMouseDragged(me));
		canvas.setOnMouseReleased(me -> mapController.handleMouseDragReleased(me));
		btAddImage.setOnAction(e -> {
			app.getDataComponent().snap();
			mapController.handleAddImage();
		});
		btAddLabel.setOnAction(e -> {
			mapController.handleAddLabel();
		});
		InvalidationListener fontListener = e -> {
			app.getDataComponent().snap();
			mapController.handleChangeFont();
		};
		fontColorPicker.valueProperty().addListener(fontListener);
		fontCombo.getSelectionModel().selectedItemProperty().addListener(fontListener);
		fontSizeCombo.getSelectionModel().selectedItemProperty().addListener(fontListener);
		btBold.selectedProperty().addListener(fontListener);
		btItalics.selectedProperty().addListener(fontListener);
		backgroundColorPicker.valueProperty().addListener(e -> {
			app.getDataComponent().snap();
			setBackgroundColor(backgroundColorPicker.getValue());
			app.getDataComponent().getMap().setBackgroundColor(backgroundColorPicker.getValue());
		});
		btBigger.setOnAction(e -> {
			app.getDataComponent().snap();
			mapController.handleGrow();
		});
		btSmaller.setOnAction(e -> {
			app.getDataComponent().snap();
			mapController.handleShrink();
		});
		app.getGUI().getPrimaryScene().setOnKeyPressed(e -> {
			switch (e.getCode()) {
				case W:
					zoomPane.panUp();
					break;
				case A:
					zoomPane.panLeft();
					break;
				case S:
					zoomPane.panDown();
					break;
				case D:
					zoomPane.panRight();
					break;
			}
		});
		btZoomIn.setOnAction(e -> zoomPane.zoomIn());
		btZoomOut.setOnAction(e -> zoomPane.zoomOut());
		btRemElement.setOnAction(e -> mapController.handleRemElement());
		btAddStatToLine.setOnAction(e -> mapController.handleAddStaToLine());
		btRemStatFromLine.setOnAction(e -> mapController.handleRemStaFromLine());
		btUndo.setOnAction(e -> {if (app.getDataComponent().canUndo()) app.getDataComponent().undo();});
		btRedo.setOnAction(e -> {if (app.getDataComponent().canRedo()) app.getDataComponent().redo();});

	}

	private void initStyle() {
		undoRedoToolbar.getStyleClass().add(CLASS_TOOLBAR_ROW);
		aboutToolbar.getStyleClass().add(CLASS_TOOLBAR_ROW);
		leftPane.getStyleClass().add(CLASS_BORDERED_PANE);
		linesToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
		stationsToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
		stationRouterToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
		decorToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
		fontToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
		navToolbar.getStyleClass().add(CLASS_BORDERED_PANE);

		linesLabel.getStyleClass().add(CLASS_LABEL);
		stationsLabel.getStyleClass().add(CLASS_LABEL);
		decorLabel.getStyleClass().add(CLASS_LABEL);
		fontLabel.getStyleClass().add(CLASS_LABEL);
		navLabel.getStyleClass().add(CLASS_LABEL);

		btBold.getStyleClass().add(CLASS_BUTTON);
		btItalics.getStyleClass().add(CLASS_BUTTON);

	}

	@Override
	public void resetWorkspace() {

	}

	@Override
	public void reloadWorkspace(AppDataComponent dataComponent) {
		MData data = (MData) dataComponent;
		app.getGUI().getWindow().setTitle(props.getProperty(APP_TITLE) + " - " + data.getMap().getName());
		enableButtons();
		if (data.getMap().getBackgroundImagePath() != null)
			setBackgroundImage(new File(data.getMap().getBackgroundImagePath()));
	}


	public void loadSettings(MapElement node) {
		app.getGUI().updateToolbarControls(false);
		enableButtons();
		switch (node.getType()) {
			case LABEL:
				popFontToolbar(node);
				break;
			case STATION:
				popFontToolbar(node);
				updateStationsToolbar(node);
				break;
			case LINE:
				popFontToolbar(node);
				updateLinesToolbar(node);
				break;
			case LINE_END:
				popFontToolbar(node);
		}
	}

	public void updateLinesToolbar(MapElement line) {
		linesCombo.getItems().clear();
		app.getDataComponent().getMap().getLines().forEach(currLine -> {
			linesCombo.getItems().add(currLine.getName());
		});
		btEditLine.setDisable(linesCombo.getItems().isEmpty());
		if (linesCombo.getItems().contains(((Line) line).getName()))
			linesCombo.getSelectionModel().select(((Line) line).getName());
		lineThicknessSlider.setValue(((Line) line).getThickness());
		btEditLine.setBackground(new Background(new BackgroundFill(((Line) line).getColor(), null, null)));
	}

	private void updateStationsToolbar(MapElement station) {
		stationsCombo.getItems().clear();
		app.getDataComponent().getMap().getAllStations().forEach(sta -> {
			stationsCombo.getItems().add(sta.getName());
		});
		if (stationsCombo.getItems().contains(((Station) station).getName()))
			stationsCombo.getSelectionModel().select(((Station) station).getName());
		stationRadiusSlider.setValue((((Station) station).getRadius()));
		stationsColorPicker.setValue(((Station) station).getColor());
	}

	private Pane hSpacer(Priority p) {
		Pane spacer = new Pane();
		HBox.setHgrow(spacer, p);
		return spacer;
	}

	public void enableButtons() {
		popRouteToolbar(app.getDataComponent().getMap().getAllStations());
		app.getGUI().updateToolbarControls(false);
		boolean isLine = false, isLineEnd = false;
		if (app.getDataComponent().getSelected() != null) {
			isLine = app.getDataComponent().getSelected().getType() == MapElement.NodeType.LINE;
			isLineEnd = app.getDataComponent().getSelected().getType() == MapElement.NodeType.LINE_END;
		}
		switch (app.getDataComponent().getState()) {

			case SELECTING:
			case ADD_LINE:
			case ADD_STATION:  //a
				btAddLine.setDisable(false);
				btRemoveLine.setDisable(true);
				btEditLine.setDisable(true);
				btAddStation.setDisable(false);
				btRemStation.setDisable(true);
				btMoveStatLabel.setDisable(stationsCombo.getItems().isEmpty());
				break;
			case DRAGGING:
			case DRAGGING_NOTHING: //b
				btMoveStatLabel.setDisable(stationsCombo.getItems().isEmpty());
				btRemoveLine.setDisable(linesCombo.getItems().isEmpty());
				btRemStation.setDisable(stationsCombo.getItems().isEmpty());
				btEditLine.setDisable(linesCombo.getItems().isEmpty());
				break;
			case FINISH_LINE:
				btAddLine.setDisable(true);
				btRemoveLine.setDisable(true);
				btEditLine.setDisable(true);
				btRemStation.setDisable(true);
				btMoveStatLabel.setDisable(true);
				btAddStation.setDisable(true);
				break;
		}
		btRemElement.setDisable(!(app.getDataComponent().getSelected() instanceof MapLabel || app.getDataComponent().getSelected() instanceof MapImage));
		btRemStatFromLine.setDisable(linesCombo.getItems().isEmpty() || stationsCombo.getItems().isEmpty());
		btAddStatToLine.setDisable(linesCombo.getItems().isEmpty() || stationsCombo.getItems().isEmpty());
		btUndo.setDisable(!app.getDataComponent().canUndo());
		btRedo.setDisable(!app.getDataComponent().canRedo());
	}

	private void popFontToolbar(MapElement selected) {
		if (selected != null && selected.getFont() != null) {
			Font font = selected.getFont();
			btBold.setSelected(font.getStyle().contains("Bold"));
			btItalics.setSelected(font.getStyle().contains("Italic"));
			fontCombo.getSelectionModel().select(font.getFamily());
			fontSizeCombo.getSelectionModel().select(new Integer((int) font.getSize()));
			switch (selected.getType()) {

				case LINE_END:
					fontColorPicker.setValue(((LineEnd) selected).getTextColor());
					break;
				case LABEL:
					fontColorPicker.setValue(((MapLabel) selected).getColor());
					break;
				case STATION:
					fontColorPicker.setValue(((Station) selected).getTextColor());
					break;
			}
		}
	}


	private void popRouteToolbar(List<Station> stations) {
		routToCombo.getItems().clear();
		routFromCombo.getItems().clear();
		stations.forEach(sta -> {
			routToCombo.getItems().add(sta.getName());
			routFromCombo.getItems().add(sta.getName());

		});
		routToCombo.getSelectionModel().selectFirst();
		routFromCombo.getSelectionModel().selectFirst();
	}

	private void popLinesToolbar(List<Line> lines) {

	}

	private void popStationsToolbar(List<Station> stations) {


	}

	public Pane getCanvas() {
		return canvas;
	}

	public void setBackgroundColor(Color color) {
		String initialHex;
		String finalString;
		initialHex = Integer.toHexString(color.hashCode()).toUpperCase();
		switch (initialHex.length()) {
			case 2:
				finalString = "000000";
				break;
			case 3:
				finalString = String.format("00000%s", initialHex.substring(0, 1));
				break;
			case 4:
				finalString = String.format("0000%s", initialHex.substring(0, 2));
				break;
			case 5:
				finalString = String.format("000%s", initialHex.substring(0, 3));
				break;
			case 6:
				finalString = String.format("00%s", initialHex.substring(0, 4));
				break;
			case 7:
				finalString = String.format("0%s", initialHex.substring(0, 5));
				break;
			default:
				finalString = initialHex.substring(0, 6);
		}
		canvas.setStyle("-fx-background-color: #" + finalString + ";");
		app.getDataComponent().getMap().setBackgroundImage(null);
		enableButtons();

	}

	public boolean setBackgroundImage(File backgroundImageFile) {
		try {
			canvas.setBackground(new Background(new BackgroundImage(SwingFXUtils.toFXImage(ImageIO.read(backgroundImageFile),
					null), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
					new BackgroundSize(AUTO, AUTO, false, false, true, false)
			)));
		} catch (Exception e) {
			return false;
		}
		app.getDataComponent().getMap().setBackgroundColor(null);
		return true;
	}

	public double getCurrentLineThickness() {
		return lineThicknessSlider.getValue();
	}

	public double getCurrentStationRadius() {
		return stationRadiusSlider.getValue();
	}

	public Color getCurrentStationColor() {
		return stationsColorPicker.getValue();
	}

	public Font getCurrentFont() {
		return Font.font(
				fontCombo.getSelectionModel().getSelectedItem(),
				btBold.isSelected() ? FontWeight.BOLD : FontWeight.NORMAL,
				btItalics.isSelected() ? FontPosture.ITALIC : FontPosture.REGULAR,
				fontSizeCombo.getSelectionModel().getSelectedItem()
		);
	}

	public Color getCurrentFontColor() {
		return fontColorPicker.getValue();
	}

	public void loadBackgroundSettings(Map map) {
		Color bgColor = map.getBackgroundColor();
		if (bgColor != null)
			backgroundColorPicker.setValue(bgColor);
		String backgroundImagePath = map.getBackgroundImagePath();
		if (backgroundImagePath != null)
			setBackgroundImage(new File(backgroundImagePath));
	}

	public void popCombos() {
		Map map = app.getDataComponent().getMap();
		linesCombo.getItems().clear();
		map.getLines().forEach(line -> linesCombo.getItems().add(line.getName()));
		stationsCombo.getItems().clear();
		map.getAllStations().forEach(sta -> stationsCombo.getItems().add(sta.getName()));

	}
}
