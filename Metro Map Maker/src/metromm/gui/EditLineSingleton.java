package metromm.gui;

import djf.ui.AppMessageDialogSingleton;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import metromm.MetroApp;
import metromm.data.Line;
import properties_manager.PropertiesManager;

import static djf.settings.AppPropertyType.APP_LOGO;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import static metromm.gui.MLangProps.*;
import static metromm.gui.MStyle.*;

public class EditLineSingleton extends Stage {
	protected static EditLineSingleton instance;

	protected MetroApp app;
	protected PropertiesManager props;
	protected VBox outerBox;
	protected HBox upperBox;
	protected HBox lowerBox;
	protected TextField nameField;
	protected Label nameLabel;
	protected ColorPicker colorPicker;
	protected Button btCancel, btConfirm;
	protected CheckBox circularCheckBox;
	protected Scene scene;
	protected String chosenName;
	protected Color chosenColor;
	protected boolean circular;


	public static EditLineSingleton getInstance() {
		if (instance == null) {
			instance = new EditLineSingleton();
		}
		return instance;
	}

	public void init(MetroApp app) {
		this.app = app;
		initOwner(app.getGUI().getWindow());
		props = PropertiesManager.getPropertiesManager();
		setTitle(props.getProperty(EDIT_LINE_TITLE.toString()));
		getIcons().add(new Image(FILE_PROTOCOL + PATH_IMAGES + props.getProperty(APP_LOGO)));
		initLayout();
		scene.getStylesheets().addAll(app.getGUI().getWindow().getScene().getStylesheets());
		initListeners();
		initStyle();
	}

	public void populate(String name, Color color, boolean circular) {
		chosenName = name;
		nameField.setText(name);
		chosenColor = color;
		colorPicker.setValue(color);
		circularCheckBox.setSelected(circular);
		this.circular = circular;
	}

	@SuppressWarnings("Duplicates")
	protected void initStyle() {
		outerBox.getStyleClass().add(CLASS_BORDERED_PANE);
		upperBox.getStyleClass().add(CLASS_TOOLBAR_ROW);
		lowerBox.getStyleClass().add(CLASS_TOOLBAR_ROW);
		nameLabel.getStyleClass().add(CLASS_LABEL);
		circularCheckBox.getStyleClass().add(CLASS_LABEL);
	}

	private void initListeners() {
		btConfirm.setOnAction(e -> {
			if (nameField.getText().equals(((Line) app.getDataComponent().getSelected()).getName()) || !(nameField.getText().equals("") || app.getDataComponent().getMap().getLines().stream().anyMatch(line -> line.getName().equals(nameField.getText())))) {
				chosenName = nameField.getText();
				chosenColor = colorPicker.getValue();
				circular = circularCheckBox.isSelected();
				close();
			} else {
				AppMessageDialogSingleton.getSingleton().show(props.getProperty(EDIT_LINE_TITLE.toString()),
						props.getProperty(ADD_LINE_INVALID.toString()));
			}
		});
		btCancel.setOnAction(e -> close());
		setOnCloseRequest(e -> close());
	}

	protected void initLayout() {
		nameLabel = new Label(props.getProperty(ADD_LINE_NAME.toString()));
		nameField = new TextField();
		nameField.setPromptText(props.getProperty(ADD_LINE_PROMPT.toString()));
		colorPicker = new ColorPicker(Color.BLACK);
		circularCheckBox = new CheckBox(props.getProperty(EDIT_LINE_CIRCULAR.toString()));
		upperBox = new HBox();
		upperBox.getChildren().addAll(hSpacer(Priority.ALWAYS),
				nameLabel, nameField, hSpacer(Priority.ALWAYS),
				circularCheckBox, hSpacer(Priority.ALWAYS),
				colorPicker, hSpacer(Priority.ALWAYS));
		lowerBox = new HBox();
		btCancel = new Button(props.getProperty(BTN_CANCEL.toString()));
		btCancel.setCancelButton(true);
		btConfirm = new Button(props.getProperty(BTN_CONFIRM.toString()));
		btConfirm.setDefaultButton(true);
		lowerBox.getChildren().addAll(hSpacer(Priority.ALWAYS), btCancel,
				hSpacer(Priority.ALWAYS), btConfirm, hSpacer(Priority.ALWAYS));
		outerBox = new VBox();
		outerBox.getChildren().addAll(vSpacer(Priority.ALWAYS),
				upperBox, vSpacer(Priority.ALWAYS),
				lowerBox, vSpacer(Priority.ALWAYS));
		scene = new Scene(outerBox);
		setScene(scene);
	}

	protected Pane hSpacer(Priority p) {
		Pane spacer = new Pane();
		HBox.setHgrow(spacer, p);
		return spacer;
	}

	protected Pane vSpacer(Priority p) {
		Pane spacer = new Pane();
		VBox.setVgrow(spacer, p);
		return spacer;
	}

	public String getChosenName() {
		return chosenName;
	}

	public Color getChosenColor() {
		return chosenColor;
	}

	public boolean isCircular() {
		return circular;
	}
}
