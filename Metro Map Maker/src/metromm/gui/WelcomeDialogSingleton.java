package metromm.gui;

import djf.ui.AppMessageDialogSingleton;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import metromm.MetroApp;
import metromm.file.JSONProps;
import properties_manager.PropertiesManager;

import java.io.File;

import static djf.settings.AppPropertyType.APP_LOGO;
import static djf.settings.AppPropertyType.APP_TITLE;
import static djf.settings.AppStartupConstants.*;
import static metromm.gui.MLangProps.*;
import static metromm.gui.MStyle.*;

@SuppressWarnings("FieldCanBeLocal")
public class WelcomeDialogSingleton extends Stage {

	private static WelcomeDialogSingleton instance;

	private MetroApp app;
	private PropertiesManager props;
	private VBox rightPane;
	private VBox leftPane;
	private ScrollPane recentWorksScroll;
	private VBox recentWorksList;
	private HBox outerPane;
	private Button btnNewMap;
	private Label recentWorkLabel;
	private ImageView appBanner;
	private Scene scene;

	private String selectedFilename;


	public static WelcomeDialogSingleton getInstance() {
		if (instance == null)
			instance = new WelcomeDialogSingleton();
		return instance;
	}

	public String showWelcomeDialog() throws Exception {
		if (app == null)
			throw new Exception("Please initialize the welcome dialog before using it!");
		showAndWait();
		if (selectedFilename == null) {
			app.getGUI().getWindow().close();
			return null;
		} else return selectedFilename.equals("") ? "Unnamed" : selectedFilename;
	}

	public void init(MetroApp app) {
		this.app = app;
		initOwner(app.getGUI().getWindow());

		props = PropertiesManager.getPropertiesManager();
		setTitle(props.getProperty(WELCOME_TITLE));
		getIcons().add(new Image(FILE_PROTOCOL + PATH_IMAGES + props.getProperty(APP_LOGO)));
		initLayout();
		scene.getStylesheets().addAll(app.getGUI().getWindow().getScene().getStylesheets());
		initListeners();
		initStyle();
	}

	private void initLayout() {
		recentWorkLabel = new Label(props.getProperty(LABEL_RECENT_WORKS));
		appBanner = new ImageView(new Image(FILE_PROTOCOL + PATH_IMAGES + props.getProperty(IMG_BANNER)));
		btnNewMap = new Button(props.getProperty(BTN_NEW_MAP));
		btnNewMap.setTooltip(new Tooltip(props.getProperty(BTN_NEW_MAP_TOOLTIP)));
		setWidth(600);
		setHeight(400);
		outerPane = new HBox();
		rightPane = new VBox();
		rightPane.setAlignment(Pos.CENTER);
		rightPane.setSpacing(100);
		rightPane.setFillWidth(false);
		rightPane.getChildren().addAll(appBanner, btnNewMap);
		leftPane = new VBox();
		recentWorksScroll = new ScrollPane();
		VBox.setVgrow(recentWorkLabel, Priority.ALWAYS);
		recentWorksList = new VBox();
		VBox.setVgrow(recentWorksList, Priority.ALWAYS);
		recentWorksScroll.setFitToHeight(true);
		recentWorksScroll.setFitToWidth(true);
		JSONProps.getInstance().getFilenames().forEach(str -> {
			Button bt = new Button(str);
			bt.setTooltip(new Tooltip(props.getProperty(LOAD_TOOLTIP_PREFIX) + " " + str));
			recentWorksList.getChildren().add(bt);
		});
		recentWorksScroll.setContent(recentWorksList);
		leftPane.setSpacing(50);
		leftPane.setAlignment(Pos.TOP_CENTER);
		leftPane.getChildren().addAll(recentWorkLabel, recentWorksScroll);
		leftPane.minWidth(250);
		leftPane.prefWidth(250);
		outerPane.getChildren().addAll(leftPane, rightPane);
		HBox.setHgrow(rightPane, Priority.ALWAYS);
		scene = new Scene(outerPane);
		setScene(scene);
	}

	@SuppressWarnings("Duplicates")
	private void initListeners() {
		btnNewMap.setOnAction(e -> {
			while (true) {
				String filename = MetroApp.showTextInputDialog(app.getGUI().getWindow(), NEW_MAP_TITLE, NEW_MAP_HEADER, NEW_MAP_PROMPT);
				if (filename == null)
					return;
				File file = new File(PATH_WORK + filename + ".mmm");
				if (filename.equals("")) {
					AppMessageDialogSingleton.getSingleton().show(props.getProperty(APP_TITLE.toString()), props.getProperty(NEW_UNNAMED_ENTERED.toString()));
				} else if (!file.exists()) {
					selectedFilename = filename;
					break;
				} else {
					AppMessageDialogSingleton.getSingleton().show(props.getProperty(APP_TITLE.toString()), props.getProperty(MAP_EXISTS_MSG.toString()));
				}
			}
			close();
		});
		recentWorksList.getChildren().forEach(node -> {
			Button btn = (Button) node;
			btn.setOnAction(e -> {
				selectedFilename = btn.getText();
				close();
			});
		});
	}

	private void initStyle() {
		outerPane.getStyleClass().add(CLASS_BORDERED_PANE);
		leftPane.getStyleClass().add(CLASS_TOOLBAR_ROW);
		rightPane.getStyleClass().add(CLASS_TOOLBAR_ROW);
		recentWorkLabel.getStyleClass().add(CLASS_LABEL);
		recentWorksScroll.getStyleClass().add(CLASS_TOOLBAR_ROW);
		recentWorksList.getChildren().forEach(node -> {
			node.getStyleClass().add(CLASS_RECENT_BUTTON);
			node.getStyleClass().remove(CLASS_BUTTON);
		});
		btnNewMap.getStyleClass().add(CLASS_BUTTON);
	}

}
