package metromm.gui;

public class MStyle {

	public static final String CLASS_BORDERED_PANE = "bordered_pane";
	public static final String CLASS_RECENT_BUTTON = "recent_works_button";
	public static final String CLASS_TOOLBAR_ROW = "edit_toolbar_row";
	public static final String CLASS_LABEL = "label_colored";
	public static final String CLASS_BUTTON = "button";

}
