
package metromm.gui;

import javafx.scene.Node;
import javafx.scene.layout.Pane;

/**
 * @author E. Stark
 * @version 20171207
 */
public class ZoomPane extends Pane {

	private Node node;

	public ZoomPane(Node node) {
		this.node = node;
		getChildren().add(node);
	}

	public void panLeft() {
		setTranslateX(getTranslateX() - 0.1 * getWidth());
	}

	public void panRight() {
		setTranslateX(getTranslateX() + 0.1 * getWidth());
	}

	public void panUp() {
		setTranslateY(getTranslateY() - 0.1 * getHeight());
	}

	public void panDown() {
		setTranslateY(getTranslateY() + 0.1 * getHeight());
	}

	public void zoomIn() {
		setScaleX(getScaleX() * 1.1);
		setScaleY(getScaleY() * 1.1);
	}

	public void zoomOut() {
		setScaleX(getScaleX() / 1.1);
		setScaleY(getScaleY() / 1.1);
	}

}
