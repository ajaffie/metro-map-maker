package metromm.gui;

import djf.ui.AppMessageDialogSingleton;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import metromm.MetroApp;
import properties_manager.PropertiesManager;

import static djf.settings.AppPropertyType.APP_LOGO;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import static metromm.gui.MLangProps.ADD_LINE_INVALID;
import static metromm.gui.MLangProps.ADD_LINE_TITLE;

public class AddLineSingleton extends EditLineSingleton {

	private static AddLineSingleton instance;

	private boolean cont = false;

	public static AddLineSingleton getInstance() {
		if (instance == null) {
			instance = new AddLineSingleton();
		}
		return instance;
	}

	public boolean canCreate() {
		if (cont) {
			cont = false;
			return true;
		} else
			return false;

	}

	public void init(MetroApp app) {
		this.app = app;
		initOwner(app.getGUI().getWindow());
		props = PropertiesManager.getPropertiesManager();
		setTitle(props.getProperty(ADD_LINE_TITLE.toString()));
		getIcons().add(new Image(FILE_PROTOCOL + PATH_IMAGES + props.getProperty(APP_LOGO)));
		initLayout();
		upperBox.getChildren().remove(upperBox.getChildren().indexOf(circularCheckBox) - 1, upperBox.getChildren().indexOf(circularCheckBox));
		scene.getStylesheets().addAll(app.getGUI().getWindow().getScene().getStylesheets());
		initListeners();
		initStyle();
	}

	private void initListeners() {
		btConfirm.setOnAction(e -> {
			if (!(nameField.getText().equals("") || app.getDataComponent().getMap().getLines().stream().anyMatch(line -> line.getName().equals(nameField.getText())))) {
				chosenName = nameField.getText();
				nameField.clear();
				chosenColor = colorPicker.getValue();
				colorPicker.setValue(Color.BLACK);
				cont = true;
				close();
			} else {
				AppMessageDialogSingleton.getSingleton().show(props.getProperty(ADD_LINE_TITLE.toString()),
						props.getProperty(ADD_LINE_INVALID.toString()));
			}
		});
		btCancel.setOnAction(e -> close());
		setOnCloseRequest(e -> close());
	}
}
