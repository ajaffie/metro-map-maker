package metromm;

import djf.AppTemplate;
import djf.ui.AppGUI;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Modality;
import javafx.stage.Stage;
import metromm.data.MData;
import metromm.file.JSONProps;
import metromm.file.MFiles;
import metromm.gui.AddLineSingleton;
import metromm.gui.EditLineSingleton;
import metromm.gui.MWorkspace;
import metromm.gui.WelcomeDialogSingleton;
import properties_manager.PropertiesManager;

import java.io.File;
import java.util.Optional;

import static djf.settings.AppPropertyType.*;
import static djf.settings.AppStartupConstants.*;
import static metromm.gui.MStyle.CLASS_BORDERED_PANE;

public class MetroApp extends AppTemplate {
	@Override
	public void buildAppComponentsHook() {
		fileComponent = new MFiles();
		dataComponent = new MData(this);
		workspaceComponent = new MWorkspace(this);
	}

	@Override
	public void start(Stage primaryStage) {
		PropertiesManager props = PropertiesManager.getPropertiesManager();
		AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
		dialog.init(primaryStage);
		try {
			boolean success = loadProperties(APP_PROPERTIES_FILE_NAME);
			if (success) {
				boolean jsonLoaded = JSONProps.getInstance().loadProps();
				if (!jsonLoaded) {
					return;
				} else {
					String appTitle = props.getProperty(APP_TITLE);
					gui = new AppGUI(primaryStage, appTitle, this);
					dialog.getScene().getStylesheets().addAll(gui.getPrimaryScene().getStylesheets());
					dialog.getScene().getRoot().getStyleClass().add(CLASS_BORDERED_PANE);
					dialog.getIcons().add(gui.getWindow().getIcons().get(0));
					AppYesNoCancelDialogSingleton ynDialog = AppYesNoCancelDialogSingleton.getSingleton();
					ynDialog.init(primaryStage);
					ynDialog.getScene().getStylesheets().addAll(gui.getPrimaryScene().getStylesheets());
					ynDialog.getScene().getRoot().getStyleClass().add(CLASS_BORDERED_PANE);
					ynDialog.getIcons().add(gui.getWindow().getIcons().get(0));
					AddLineSingleton.getInstance().init(this);
					EditLineSingleton.getInstance().init(this);
					WelcomeDialogSingleton wd = WelcomeDialogSingleton.getInstance();
					wd.init(this);
					String filename = wd.showWelcomeDialog();
					buildAppComponentsHook();
					if (filename == null) {
						workspaceComponent.activateWorkspace(gui.getAppPane());
						primaryStage.show();
					} else if (new File(PATH_WORK + filename + ".mmm").exists()) {
						fileComponent.loadData(dataComponent, filename);
						getGUI().getFileController().setCurrentWorkFile(new File(PATH_WORK + filename + ".mmm"));
						workspaceComponent.resetWorkspace();
						workspaceComponent.reloadWorkspace(dataComponent);
						workspaceComponent.activateWorkspace(gui.getAppPane());
						primaryStage.show();
					} else {
						((MData) dataComponent).newMap(filename);
						workspaceComponent.resetWorkspace();
						workspaceComponent.reloadWorkspace(dataComponent);
						workspaceComponent.activateWorkspace(gui.getAppPane());
						fileComponent.saveData(dataComponent, PATH_WORK + filename + ".mmm");
						primaryStage.show();
						dialog.show(props.getProperty(NEW_COMPLETED_TITLE), props.getProperty(NEW_COMPLETED_MESSAGE));
					}
				}
			}
		} catch (Exception e) {
			dialog.show(PROPERTIES_FILE_ERROR_TITLE, PROPERTIES_FILE_ERROR_MESSAGE);

		}
	}

	@Override
	public MData getDataComponent() {
		return (MData) dataComponent;
	}

	@Override
	public MWorkspace getWorkspaceComponent() {
		return (MWorkspace) workspaceComponent;
	}

	public static String showTextInputDialog(Stage parent, Object titleProperty, Object headerProperty, Object contentProperty) {
		PropertiesManager props = PropertiesManager.getPropertiesManager();
		String title = props.getProperty(titleProperty);
		String headerText = props.getProperty(headerProperty);
		String contentText = props.getProperty(contentProperty);
		TextInputDialog textDialog = new TextInputDialog();
		textDialog.getDialogPane().getStyleClass().add(CLASS_BORDERED_PANE);
		textDialog.getDialogPane().getChildren().get(0).setStyle("-fx-background-color: #1b82e0");
		textDialog.initOwner(parent);
		textDialog.initModality(Modality.APPLICATION_MODAL);
		textDialog.setTitle(title);
		textDialog.setHeaderText(headerText);
		textDialog.setContentText(contentText);
		Optional<String> result = textDialog.showAndWait();
		return result.orElse(null);
	}
}
